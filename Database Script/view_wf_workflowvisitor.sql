CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `view_wf_workflowvisitor` AS
    SELECT 
        `v`.`id` AS `id`,
        `v`.`name` AS `name`,
        `v`.`icnumber` AS `icnumber`,
        `v`.`time_stamp` AS `time_stamp`,
        `v`.`from_date` AS `from_date`,
        `v`.`to_date` AS `to_date`,
        `v`.`status` AS `status`,
        `po`.`purposename` AS `purposename`,
        `de`.`departmentname` AS `departmentname`
    FROM
        ((`visitors` `v`
        LEFT JOIN `purpose` `po` ON ((`po`.`id` = `v`.`visit_purpose`)))
        LEFT JOIN `department` `de` ON ((`de`.`id` = `v`.`visit_department`)))