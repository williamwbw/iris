-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: iris_middleman
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `workflow_setup`
--

DROP TABLE IF EXISTS `workflow_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workflow_setup` (
  `id` int NOT NULL AUTO_INCREMENT,
  `conditions` varchar(255) DEFAULT NULL,
  `createdatetime` varchar(255) DEFAULT NULL,
  `createuserid` int DEFAULT NULL,
  `modifydatetime` varchar(255) DEFAULT NULL,
  `modifyuserid` int DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `parentid` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdb7f9jkuk89de2uuopt9ygf0j` (`parentid`),
  CONSTRAINT `FKdb7f9jkuk89de2uuopt9ygf0j` FOREIGN KEY (`parentid`) REFERENCES `workflow_setup` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workflow_setup`
--

LOCK TABLES `workflow_setup` WRITE;
/*!40000 ALTER TABLE `workflow_setup` DISABLE KEYS */;
INSERT  IGNORE INTO `workflow_setup` (`id`, `conditions`, `createdatetime`, `createuserid`, `modifydatetime`, `modifyuserid`, `status`, `parentid`) VALUES (1,'Purpose=1',NULL,NULL,NULL,NULL,'active',NULL),(2,'Department=1',NULL,NULL,NULL,NULL,'active',NULL),(3,'Purpose=1ANDDepartment=1','2021-04-19 15:16:09',NULL,'2021-04-19 15:16:09',NULL,'active',NULL),(4,'Purpose=2','2021-04-23 12:01:03',NULL,'2021-04-23 12:01:03',NULL,'Active',NULL),(12,'Department=2','2021-04-25 17:11:45',NULL,'2021-04-26 11:43:11',NULL,'Active',NULL),(13,'purpose=1','2021-04-26 15:56:06',NULL,'2021-04-26 15:56:06',NULL,'Active',NULL),(14,NULL,'2021-04-26 16:23:39',NULL,'2021-04-26 16:23:39',NULL,'Active',NULL),(15,'Purpose=2 AND Department=2','2021-04-26 18:43:15',3,'2021-04-26 18:43:15',3,'Active',NULL),(16,'Purpose=2 AND Department=2','2021-04-26 18:45:00',3,'2021-04-26 18:45:00',3,'Active',NULL);
/*!40000 ALTER TABLE `workflow_setup` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-24 22:47:27
