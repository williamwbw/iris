CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `view_crud_getempl_positionrole` AS
    SELECT 
        `employees`.`id` AS `id`,
        CONCAT(`employees`.`name`,
                ',  ',
                '(Role: ',
                `empl_role`.`rolename`,
                ' ;  ',
                'Position: ',
                `empl_position`.`positionname`,
                ')') AS `employeename`
    FROM
        ((`employees`
        JOIN `empl_role` ON ((`employees`.`role` = `empl_role`.`id`)))
        JOIN `empl_position` ON ((`employees`.`position` = `empl_position`.`id`)))