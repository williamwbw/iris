-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: iris_middleman
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `view_crud_getempl_positionrole`
--

DROP TABLE IF EXISTS `view_crud_getempl_positionrole`;
/*!50001 DROP VIEW IF EXISTS `view_crud_getempl_positionrole`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_crud_getempl_positionrole` AS SELECT 
 1 AS `id`,
 1 AS `employeename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_wf_workflowlog`
--

DROP TABLE IF EXISTS `view_wf_workflowlog`;
/*!50001 DROP VIEW IF EXISTS `view_wf_workflowlog`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_wf_workflowlog` AS SELECT 
 1 AS `logid`,
 1 AS `action`,
 1 AS `remarks`,
 1 AS `visitorid`,
 1 AS `createdatetime`,
 1 AS `modifyuserid`,
 1 AS `positionname`,
 1 AS `visitorname`,
 1 AS `employeename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_report_workflowlog`
--

DROP TABLE IF EXISTS `view_report_workflowlog`;
/*!50001 DROP VIEW IF EXISTS `view_report_workflowlog`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_report_workflowlog` AS SELECT 
 1 AS `id`,
 1 AS `action`,
 1 AS `remarks`,
 1 AS `createdatetime`,
 1 AS `visitorid`,
 1 AS `employeename`,
 1 AS `positionname`,
 1 AS `visitorname`,
 1 AS `timestamp`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_wf_workflowsetup`
--

DROP TABLE IF EXISTS `view_wf_workflowsetup`;
/*!50001 DROP VIEW IF EXISTS `view_wf_workflowsetup`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_wf_workflowsetup` AS SELECT 
 1 AS `id`,
 1 AS `title`,
 1 AS `wf_id`,
 1 AS `status`,
 1 AS `employeename`,
 1 AS `purposename`,
 1 AS `departmentname`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_report_checkinout`
--

DROP TABLE IF EXISTS `view_report_checkinout`;
/*!50001 DROP VIEW IF EXISTS `view_report_checkinout`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_report_checkinout` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `icnumber`,
 1 AS `timein`,
 1 AS `timeout`,
 1 AS `departmentname`,
 1 AS `purposename`,
 1 AS `intimestamp`,
 1 AS `outtimestamp`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_wf_workflowsetup_nofilter`
--

DROP TABLE IF EXISTS `view_wf_workflowsetup_nofilter`;
/*!50001 DROP VIEW IF EXISTS `view_wf_workflowsetup_nofilter`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_wf_workflowsetup_nofilter` AS SELECT 
 1 AS `id`,
 1 AS `title`,
 1 AS `wf_id`,
 1 AS `status`,
 1 AS `employeename`,
 1 AS `purpose`,
 1 AS `department`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_wf_workflowvisitor`
--

DROP TABLE IF EXISTS `view_wf_workflowvisitor`;
/*!50001 DROP VIEW IF EXISTS `view_wf_workflowvisitor`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_wf_workflowvisitor` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `icnumber`,
 1 AS `time_stamp`,
 1 AS `from_date`,
 1 AS `to_date`,
 1 AS `status`,
 1 AS `purposename`,
 1 AS `departmentname`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_wf_workflow_log`
--

DROP TABLE IF EXISTS `view_wf_workflow_log`;
/*!50001 DROP VIEW IF EXISTS `view_wf_workflow_log`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_wf_workflow_log` AS SELECT 
 1 AS `logid`,
 1 AS `action`,
 1 AS `remarks`,
 1 AS `visitorid`,
 1 AS `employeename`,
 1 AS `positionname`,
 1 AS `visitorname`,
 1 AS `createdatetime`,
 1 AS `modifyuserid`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_wf_workflowemp`
--

DROP TABLE IF EXISTS `view_wf_workflowemp`;
/*!50001 DROP VIEW IF EXISTS `view_wf_workflowemp`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_wf_workflowemp` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `role`,
 1 AS `position`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_crud_getemployee`
--

DROP TABLE IF EXISTS `view_crud_getemployee`;
/*!50001 DROP VIEW IF EXISTS `view_crud_getemployee`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_crud_getemployee` AS SELECT 
 1 AS `id`,
 1 AS `emplonumber`,
 1 AS `username`,
 1 AS `name`,
 1 AS `contactnumber`,
 1 AS `status`,
 1 AS `email`,
 1 AS `departmentname`,
 1 AS `positionname`,
 1 AS `rolename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_crud_getvisitor`
--

DROP TABLE IF EXISTS `view_crud_getvisitor`;
/*!50001 DROP VIEW IF EXISTS `view_crud_getvisitor`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_crud_getvisitor` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `icnumber`,
 1 AS `time_stamp`,
 1 AS `from_date`,
 1 AS `to_date`,
 1 AS `status`,
 1 AS `purposename`,
 1 AS `departmentname`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_crud_getempl_positionrole`
--

/*!50001 DROP VIEW IF EXISTS `view_crud_getempl_positionrole`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_crud_getempl_positionrole` AS select 1 AS `id`,1 AS `employeename` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_wf_workflowlog`
--

/*!50001 DROP VIEW IF EXISTS `view_wf_workflowlog`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_wf_workflowlog` AS select 1 AS `logid`,1 AS `action`,1 AS `remarks`,1 AS `visitorid`,1 AS `createdatetime`,1 AS `modifyuserid`,1 AS `positionname`,1 AS `visitorname`,1 AS `employeename` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_report_workflowlog`
--

/*!50001 DROP VIEW IF EXISTS `view_report_workflowlog`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_report_workflowlog` AS select 1 AS `id`,1 AS `action`,1 AS `remarks`,1 AS `createdatetime`,1 AS `visitorid`,1 AS `employeename`,1 AS `positionname`,1 AS `visitorname`,1 AS `timestamp` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_wf_workflowsetup`
--

/*!50001 DROP VIEW IF EXISTS `view_wf_workflowsetup`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_wf_workflowsetup` AS select 1 AS `id`,1 AS `title`,1 AS `wf_id`,1 AS `status`,1 AS `employeename`,1 AS `purposename`,1 AS `departmentname` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_report_checkinout`
--

/*!50001 DROP VIEW IF EXISTS `view_report_checkinout`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_report_checkinout` AS select 1 AS `id`,1 AS `name`,1 AS `icnumber`,1 AS `timein`,1 AS `timeout`,1 AS `departmentname`,1 AS `purposename`,1 AS `intimestamp`,1 AS `outtimestamp` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_wf_workflowsetup_nofilter`
--

/*!50001 DROP VIEW IF EXISTS `view_wf_workflowsetup_nofilter`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_wf_workflowsetup_nofilter` AS select 1 AS `id`,1 AS `title`,1 AS `wf_id`,1 AS `status`,1 AS `employeename`,1 AS `purpose`,1 AS `department` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_wf_workflowvisitor`
--

/*!50001 DROP VIEW IF EXISTS `view_wf_workflowvisitor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_wf_workflowvisitor` AS select 1 AS `id`,1 AS `name`,1 AS `icnumber`,1 AS `time_stamp`,1 AS `from_date`,1 AS `to_date`,1 AS `status`,1 AS `purposename`,1 AS `departmentname` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_wf_workflow_log`
--

/*!50001 DROP VIEW IF EXISTS `view_wf_workflow_log`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_wf_workflow_log` AS select 1 AS `logid`,1 AS `action`,1 AS `remarks`,1 AS `visitorid`,1 AS `employeename`,1 AS `positionname`,1 AS `visitorname`,1 AS `createdatetime`,1 AS `modifyuserid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_wf_workflowemp`
--

/*!50001 DROP VIEW IF EXISTS `view_wf_workflowemp`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_wf_workflowemp` AS select 1 AS `id`,1 AS `name`,1 AS `role`,1 AS `position` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_crud_getemployee`
--

/*!50001 DROP VIEW IF EXISTS `view_crud_getemployee`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_crud_getemployee` AS select 1 AS `id`,1 AS `emplonumber`,1 AS `username`,1 AS `name`,1 AS `contactnumber`,1 AS `status`,1 AS `email`,1 AS `departmentname`,1 AS `positionname`,1 AS `rolename` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_crud_getvisitor`
--

/*!50001 DROP VIEW IF EXISTS `view_crud_getvisitor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_crud_getvisitor` AS select 1 AS `id`,1 AS `name`,1 AS `icnumber`,1 AS `time_stamp`,1 AS `from_date`,1 AS `to_date`,1 AS `status`,1 AS `purposename`,1 AS `departmentname` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'iris_middleman'
--

--
-- Dumping routines for database 'iris_middleman'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-24 22:47:27
