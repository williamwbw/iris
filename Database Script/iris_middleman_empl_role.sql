-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: iris_middleman
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `empl_role`
--

DROP TABLE IF EXISTS `empl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empl_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `createdatetime` varchar(255) DEFAULT NULL,
  `createuserid` int DEFAULT NULL,
  `modifydatetime` varchar(255) DEFAULT NULL,
  `modifyuserid` int DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `rolename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empl_role`
--

LOCK TABLES `empl_role` WRITE;
/*!40000 ALTER TABLE `empl_role` DISABLE KEYS */;
INSERT  IGNORE INTO `empl_role` (`id`, `createdatetime`, `createuserid`, `modifydatetime`, `modifyuserid`, `status`, `rolename`) VALUES (1,NULL,NULL,NULL,NULL,'Active','Manager'),(2,NULL,NULL,NULL,NULL,'Active','admin'),(3,NULL,NULL,NULL,NULL,'Active','user'),(4,'2021-04-22 15:42:35',NULL,'2021-04-22 15:42:35',NULL,'Active','head'),(5,'2021-04-30 17:24:42',3,'2021-04-30 17:24:48',3,'Inactive','asasaaa'),(6,'2021-04-30 18:20:34',3,'2021-04-30 18:20:41',3,'Inactive','asas123');
/*!40000 ALTER TABLE `empl_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-24 22:47:27
