-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: iris_middleman
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employees` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `department` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `modifydatetime` datetime DEFAULT NULL,
  `createdatetime` datetime DEFAULT NULL,
  `createuserid` varchar(255) DEFAULT NULL,
  `modifyuserid` varchar(255) DEFAULT NULL,
  `position` int DEFAULT NULL,
  `role` int DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contactnumber` varchar(255) DEFAULT NULL,
  `emplonumber` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl4s6jpv0v81oryajvr60h9i4h` (`position`),
  KEY `FK52abfuhlgindduh6b5fhswef2` (`role`),
  CONSTRAINT `FK52abfuhlgindduh6b5fhswef2` FOREIGN KEY (`role`) REFERENCES `empl_role` (`id`),
  CONSTRAINT `FKl4s6jpv0v81oryajvr60h9i4h` FOREIGN KEY (`position`) REFERENCES `empl_position` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT  IGNORE INTO `employees` (`id`, `username`, `name`, `department`, `status`, `modifydatetime`, `createdatetime`, `createuserid`, `modifyuserid`, `position`, `role`, `email`, `contactnumber`, `emplonumber`) VALUES (1,'admin','admin','1','Active','2021-03-31 10:14:13','2021-03-29 17:11:37',NULL,'4',1,1,'abc@gmail.com','012-3456789','A0001'),(2,'wei987','jiawei23232','2','Active','2021-03-29 17:11:37','2021-03-29 17:11:37',NULL,NULL,1,1,'abc@gmail.com','012-3456789','A0002'),(40,'lim','lim','1','Active','2021-03-30 10:28:21','2021-03-29 17:11:37','4','4',5,1,'abc@gmail.com','012-3456789','A0003'),(41,'213123123','123123','1','Active','2021-03-29 17:11:37','2021-03-29 17:11:37','4','4',4,2,'abc@gmail.com','012-3456789','A0004'),(42,'123123123','23232323','1','Inactive','2021-04-30 23:10:05','2021-03-29 17:11:37','4','3',2,1,'abc@gmail.com','012-3456789','A0005'),(43,'123111111111111','1111111111111111111','2','Inactive','2021-03-31 10:21:38','2021-03-29 17:13:33','4','4',2,2,'abc@gmail.com','012-3456789','A0006'),(44,'123','123','2','Inactive','2021-03-31 10:21:10','2021-03-30 10:04:27','4','4',2,1,'abc@gmail.com','012-3456789','A0007'),(45,'123','1234567','2','Inactive','2021-03-31 10:15:08','2021-03-30 10:28:30','4','4',3,2,'abc@gmail.com','012-3456789','A0008'),(46,'rtr','rtr','1','Active','2021-03-31 15:05:49','2021-03-31 15:05:49','3','3',3,3,'abc@gmail.com','012-3456789','A0009'),(47,'lim','lim','1','Active','2021-03-31 15:05:49','2021-03-31 15:05:49','3','3',4,3,'abc@gmail.com','012-3456789','A0010'),(48,'asaaaa','asaaaa','1','Inactive','2021-04-30 18:26:34','2021-04-30 18:02:40','3','3',2,1,'abc@gmail.com','012-3456789','A0011'),(49,'ss','ss','1','Active','2021-04-30 23:10:00','2021-04-30 23:10:00','3','3',2,2,'abc@gmail.com','012-3456789','A0012'),(50,'aaaaaa','aaa','1','Inactive','2021-05-04 15:34:36','2021-05-04 15:33:16','3','3',1,1,'abc@gmail.com','012-3456789','A0013'),(51,'user','sd','1','Inactive','2021-05-04 16:36:03','2021-05-04 16:35:46','3','3',2,1,'jiawei@secure365.com.my','01123456789','a0001');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-24 22:47:26
