CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `view_wf_workflowsetup` AS
    SELECT 
        `wf`.`id` AS `id`,
        `wf`.`title` AS `title`,
        `wf`.`wf_id` AS `wf_id`,
        `wf`.`status` AS `status`,
        GROUP_CONCAT(`em`.`name`
            SEPARATOR ',') AS `employeename`,
        `po`.`purposename` AS `purposename`,
        `de`.`departmentname` AS `departmentname`
    FROM
        (((`wfsetup_details` `wf`
        LEFT JOIN `employees` `em` ON ((0 <> FIND_IN_SET(`em`.`id`, `wf`.`employeeids`))))
        LEFT JOIN `purpose` `po` ON ((SUBSTRING_INDEX(SUBSTRING_INDEX(`wf`.`conditions`, 'Purpose=', -(1)), 'AND', 1) = `po`.`id`)))
        LEFT JOIN `department` `de` ON ((SUBSTRING_INDEX(SUBSTRING_INDEX(`wf`.`conditions`, 'Department=', -(1)), 'AND', 1) = `de`.`id`)))
    GROUP BY `wf`.`id`