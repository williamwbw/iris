CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `view_wf_workflow_log` AS
    SELECT 
        `wl`.`id` AS `logid`,
        `wl`.`action` AS `action`,
        `wl`.`remarks` AS `remarks`,
        `wt`.`visitorid` AS `visitorid`,
        `em`.`name` AS `employeename`,
        `po`.`positionname` AS `positionname`,
        `vt`.`name` AS `visitorname`,
        `wl`.`createdatetime` AS `createdatetime`,
        `wl`.`modifyuserid` AS `modifyuserid`
    FROM
        ((((((`workflow_log` `wl`
        LEFT JOIN `workflow_transaction` `wt` ON (((`wl`.`ref_id` = `wt`.`id`)
            AND (`wl`.`ref_table` = 'Workflow Transaction'))))
        LEFT JOIN `wfsetup_details` `wf` ON ((`wt`.`wfsetup_id` = `wf`.`id`)))
        LEFT JOIN `users` `us` ON ((`wl`.`modifyuserid` = `us`.`id`)))
        LEFT JOIN `employees` `em` ON ((`us`.`employeeid` = `em`.`id`)))
        LEFT JOIN `empl_position` `po` ON ((`em`.`position` = `po`.`id`)))
        LEFT JOIN `visitors` `vt` ON ((`wt`.`visitorid` = `vt`.`id`)))
    WHERE
        ((`wl`.`action` = 'Approve')
            OR (`wl`.`action` = 'Reject'))
    GROUP BY `wl`.`id`