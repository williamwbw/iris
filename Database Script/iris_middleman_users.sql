-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: iris_middleman
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `createdatetime` varchar(255) DEFAULT NULL,
  `createuserid` int DEFAULT NULL,
  `modifydatetime` varchar(255) DEFAULT NULL,
  `modifyuserid` int DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `employeeid` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKr43af9ap4edm43mmtq01oddj6` (`username`),
  UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT  IGNORE INTO `users` (`id`, `email`, `password`, `username`, `access_token`, `createdatetime`, `createuserid`, `modifydatetime`, `modifyuserid`, `status`, `accessToken`, `employeeid`) VALUES (1,'lim@g.com','qwe123','lim',NULL,NULL,NULL,'2021-04-05 13:25:17',NULL,'Active',NULL,NULL),(2,'jiawei@g.com','$2a$10$DGm2fHDy7lbf99U86g5Yf.oj7mcm8Ib6MTC0.EopgaKcrGErgmi9S','mod','eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtb2QiLCJpYXQiOjE2MTcyNDc2MDksImV4cCI6MTYxNzI0NzkwOX0.kC-Zr6nlZ6KU3W168Z_19-uRshLXrSxBG7SXp2B1T4bb532iTvTzYmsiiyNhEWF7RdEVogHBLkc-cF-D6rGEfg',NULL,NULL,NULL,NULL,'Active',NULL,1),(3,'user@g.com','$2a$10$.mUxxQAWQ1Z60Smz3IJTyORUgKJkqQVPIJA4cRk0Gn3e5LpMXeUoi','user','eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiaWF0IjoxNjIxNDk0ODgxLCJleHAiOjE2MjE1MDIwODF9.4yUW1PqA_OAua04n7gmvf7bkOgkjsq4U0_NJNswQtc_YYCAETmrJraIGvPkLfcLpU3C2_3OJmrWzYRlFOeb0yg',NULL,NULL,NULL,NULL,'Active',NULL,NULL),(8,'admin@g1.com','$2a$10$WP5ctU/xzyWat.WsK2xEZ.KClkrmdjhbRw1Ko5e/ULuqZkuqc4qEy','admin123','eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbjEyMyIsImlhdCI6MTYyMTIzNTMwOSwiZXhwIjoxNjIxMjQyNTA5fQ.G9C6TKc463SVGE_VIdceFM1fFqDcHK3my8XtGtI2xVHma0gqOXasl3p3MfrOxU687s_qB_6W4YJIMOVUhYis4A',NULL,NULL,NULL,NULL,'Active',NULL,2),(9,'hogl-wm17@student.tarc.edu.my','$2a$10$4wBkTxJk2oxawn6.J45Gru0ZLoOmkVkYQG4X7CUDGWS1Fpn6J/LXC','testoh',NULL,NULL,NULL,'2021-04-06 14:44:59',96,'Active',NULL,NULL),(83,'jiawei@secure365.com.my','$2a$10$aP.8JPOIs8.YEiRwitd0fezP0d/T3XQdwU/8Q5MLNoT4TCjT8RGHa','ferfr',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(84,'jiawei11@secure365.com.my','$2a$10$fA8iAn5jl3XVQHIKlbIyse4cxD5XoNTqpY.I78YmHCN8XUq.nS3t6','rtr',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(85,'jiawei@gmail.com','$2a$10$HLcor2oiJ/ZInH5rYt5fGOej/03YSOMw9V5pJHHjPSH2NooUpM2p.','sds',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(86,'sdsd','$2a$10$4XFCQfZipA2VabGYO23M6O34FvEG.W0RVOrf9lYsHG2zvBjv8qd2a','sdsd',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(87,'dwdw','$2a$10$jkg.iQatq6YKgwzWbdYGcu7auyqSyj6obHllDBfFG7rY6Kn.Jy4uu','swdwd',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(88,'sdds','$2a$10$kq3Awjbu67iC7R.lhzUG6u8mzFzglTeimvKLbuERJzt162XSAwTNy','dsdsd',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(89,'gtgt','$2a$10$6rHZ74q0elEmxnvGNlehO.E7.bYa2LJE4JfKpjLLreBF.0mhZZdeu','tgtgtg',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(90,'asas','$2a$10$KwBeY7HvhXgEbpH/AWFEdu4k3YHTZ/i/lBknApqizFSyQQtGrQnlK','asas',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(91,'abcd','$2a$10$7md9gTAeqO004MZ9qzhGVu.jD.HOSIrduSv5iPg6XckUJ765Oh7Lq','abcd',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(92,'sdsdsdsdsdsds','$2a$10$oiZBFs.0hrzkEUeL0BMiu.jyNFbYj0wxydlYt3oW6ujgCYEaT3/OW','sdsdsdsdsdsd',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(93,'sdsdsdsdsdsdssss','$2a$10$SfJiKF5fwzpipMX6NBGFU.bNW1KTD/FThOn1YsTX4FZhuYi2vUPl6','sdsdsdsdsdsdsss',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(94,'xcx','$2a$10$/xVuDxaA7wflIXEGJ0xB8eozbjr27DQxk5G7olVl1g0OWFuddfpcG','xcx',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(96,'changging@g.com','$2a$10$HIb/i04flf44v2rXW0WrDeZPxA6rhI6MKFZQw5VwAZ7agfWAXjB2C','admin',NULL,NULL,NULL,'2021-04-06 14:42:31',8,'Active',NULL,NULL),(97,'limmffm@g.com','$2a$10$kCBgshsKQK52b6LLbTmNCeixqHjYD.PpC9pkZDBSL9Md/j0FhXsP.','teafflim',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(98,'limmffzzm@g.com','$2a$10$NE1/fjHD4X3nQ6QYTAcOxOLVJv4aKPTb26kK8e7GVhbzjcN4fyEQa','teafzzflim',NULL,NULL,NULL,'2021-04-30 18:13:31',3,'Inactive',NULL,NULL),(99,'liffzzm@g.com','$2a$10$c0e8MeY2ovTtad2puW1UR.1tUjzN4eYEi0e30yPKSLjzFm8aJ46Dq','tfzzflim',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(100,'cuba@g.com','$2a$10$pdocugb381pkwRm9F6WnI..TDAYONEJQrpchwZuj9gbMeP86Cbk/q','cuba',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(101,'zoo1111@g.com','$2a$10$zmX/qcQPYS7lnfN5W8qYNOnmIzatp9on4YtTurk3fqdEhrkPXVIzq','zoo1111',NULL,NULL,NULL,'2021-04-05 15:26:47',NULL,'Active',NULL,NULL),(102,'zoo122@g.com','$2a$10$8I4fDd69t.hBO5BzTzgteuEZelXLyNZrDMO/hr9JGeZXfA6ZLsNlW','zooo122',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(103,'zooo1@g.com','$2a$10$.9ZSyRpKxUglimRq/uFtjeClQubwyTaJazr6wnaZbOV1PZEZxZAxG','zooo1',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(104,'testqwerrr@g.com','$2a$10$ygaXnMBe6.1X0W7RyHfZv.BpJIoiTzbU.EPShKHytk8Hff.ZrULIm','testqwerrr',NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL),(105,'jkljkl@g.com','$2a$10$ADKpJJ4xYyvr6recsbWVseWOsiSsg/uruRAwha00NMjlTIBf5L4WK','jkljkl',NULL,NULL,NULL,'2021-04-30 17:50:00',3,'Inactive',NULL,NULL),(107,'aaqqq','$2a$10$v37VFbPLyx0DcwOGAD9CtOzgGwmeaWupyb7MNCNQbCmyUiy72JP9u','aaqqq',NULL,'2021-04-30 18:15:29',3,'2021-04-30 18:15:41',3,'Inactive',NULL,NULL),(108,'aa','$2a$10$iY/jHoNzgteqxocDnmHRfuVemzXoUEdpyXf2.0qeWRbdw1lMHnmSK','aa',NULL,'2021-04-30 23:19:08',3,'2021-04-30 23:19:12',3,'Inactive',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-24 22:47:26
