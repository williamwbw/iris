CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `view_wf_workflowemp` AS
    SELECT 
        `employees`.`id` AS `id`,
        `employees`.`name` AS `name`,
        `employees`.`role` AS `role`,
        `employees`.`position` AS `position`
    FROM
        ((`employees`
        JOIN `empl_role` ON ((`employees`.`role` = `empl_role`.`id`)))
        JOIN `empl_position` ON ((`employees`.`position` = `empl_position`.`id`)))