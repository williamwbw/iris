CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `view_crud_getemployee` AS
    SELECT 
        `emp`.`id` AS `id`,
        `emp`.`emplonumber` AS `emplonumber`,
        `emp`.`username` AS `username`,
        `emp`.`name` AS `name`,
        `emp`.`contactnumber` AS `contactnumber`,
        `emp`.`status` AS `status`,
        `emp`.`email` AS `email`,
        `de`.`departmentname` AS `departmentname`,
        `po`.`positionname` AS `positionname`,
        `ro`.`rolename` AS `rolename`
    FROM
        (((`employees` `emp`
        LEFT JOIN `department` `de` ON ((`emp`.`department` = `de`.`id`)))
        LEFT JOIN `empl_position` `po` ON ((`emp`.`position` = `po`.`id`)))
        LEFT JOIN `empl_role` `ro` ON ((`emp`.`role` = `ro`.`id`)))
    GROUP BY `emp`.`id`