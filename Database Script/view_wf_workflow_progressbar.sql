CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `view_wf_workflow_progressbar` AS
    SELECT 
        `wt`.`id` AS `id`,
        `wt`.`status` AS `status`,
        `wt`.`visitorid` AS `visitorid`,
        `wt`.`wfsetup_id` AS `wfsetup_id`,
        `wt`.`stage` AS `stage`,
        `wf`.`employeeids` AS `EmployeeID`,
        GROUP_CONCAT(`em`.`name`
            SEPARATOR ',') AS `Employee`,
        `po`.`positionname` AS `positionname`
    FROM
        (((`workflow_transaction` `wt`
        LEFT JOIN `wfsetup_details` `wf` ON ((`wt`.`wfsetup_id` = `wf`.`id`)))
        LEFT JOIN `employees` `em` ON ((0 <> FIND_IN_SET(`em`.`id`, `wf`.`employeeids`))))
        LEFT JOIN `empl_position` `po` ON ((`em`.`position` = `po`.`id`)))
    GROUP BY `wt`.`id`