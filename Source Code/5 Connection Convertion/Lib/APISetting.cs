﻿using Microsoft.Extensions.Configuration;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace IRISTestConnection.Lib
{
    public class APISetting
    {
        public IConfiguration configuration { get; set; }
        public APISetting(IConfiguration iconfig)
        {
            configuration = iconfig;
        }

        private string setting_name;
        private string type;
        private string value;

        public async Task<string> GetSettingAsync(string setting_name, string type)
        {

           // string value = "";

            try
            {
                using var connection = new MySqlConnection();
                await connection.OpenAsync();

                using var command = new MySqlCommand("select * from api_setting where setting_name = '" + setting_name + "' and type = '" + type + "'", connection);
                using var reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {

                    var settingName = reader.GetValue(0);
                    //var type_name = reader.GetValue(1);
                    // var value_name = reader.GetValue(value);
                    // int no_of_day = (int)cmd.ExecuteScalar();
                    // string value = (string)command.ExecuteScalar();
                    //  Output = Output

                    // var value = reader.GetValue(3);
                    Console.WriteLine("Setting Name: " + settingName + "");
                    //   Console.WriteLine("Setting Name: " + setting_name + " " + "Type: " + type + " " + "Value: " + value);          

                }

                await connection.CloseAsync();

            }
            catch (Exception ex)
            {
                return ex.InnerException.Message;
            }

            return value;
        }

        internal string GetValue(string setting_name, string type)
        {
            string value = "";
            try
            {

                using (var conn = new MySqlConnection(configuration.GetSection("ConnectionStrings").GetSection("Default").Value))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand("Select value from api_setting where setting_name='"+setting_name+"' and type='"+type+"'", conn))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                value = reader[0].ToString();
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
            }

            return value;
                //throw new NotImplementedException();
        }
    }

}

    
