﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MySqlConnector;
using IRISTestConnection.Lib;
using Microsoft.Extensions.Configuration;

namespace IRISTestConnection.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AMSEmployeeController : Controller
    {
        public IConfiguration config { get; }
        public AMSEmployeeController(IConfiguration iConfig)
        {
            config = iConfig;
        }
        [HttpPost]
        public async Task<string> GetAsync(string Token)
        {
            
            try
            {
                var Data = "";
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    Data = await reader.ReadToEndAsync();
                }

                APISetting objSetting = new APISetting(config);
                string amsemployeedomain = objSetting.GetValue("AMSEmployeeDomain", "domain");

                //dapatkan value AMSEmployee Domain here
                var client = new RestClient(amsemployeedomain);
                // client.Authenticator = new HttpBasicAuthenticator(username, password);
                var request = new RestRequest("/ams-company/ams-attendancetest/user");
                request.AddHeader("Authorization", "Bearer " + Token);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", Data, ParameterType.RequestBody);
                var response = client.Post(request);
                var content = response.Content; // Raw content as string
                return content;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
