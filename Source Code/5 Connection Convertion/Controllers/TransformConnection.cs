﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using StringBuilder = System.Text.StringBuilder;
using MySqlConnector;
using IRISTestConnection.Lib;
using Microsoft.Extensions.Configuration;

namespace IRISTestConnection.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransformConnection : ControllerBase
    {
        public IConfiguration config { get; }
        public TransformConnection(IConfiguration iConfig)
        {
            config = iConfig;
        }
        [HttpPost]
        public async Task<string> GetAsync(string ContentType, string Parameter, string Data)
        {

            APISetting objSetting = new APISetting(config);
            var keys = Request.Form.Files[0];


            string app_key = objSetting.GetValue("SecuritySettingAppKey", "App_key");
            string tcdomain = objSetting.GetValue("TransformConnectionDomain", "domain");
            var client = new RestClient(app_key);

            //dapatkan app key from db
            //string app_key = "fbdff3fc15d13e2b";
            DateTime now = DateTime.UtcNow;
            long unixTimeMilliseconds = new DateTimeOffset(now).ToUnixTimeMilliseconds();
            string timesStamp = unixTimeMilliseconds.ToString();
            //dapatkan app secret from db
            string app_secret = objSetting.GetValue("SecuritySettingAppSecret", "App_secret");
          ///  string app_secret = "dfdaba65c8b29dea850bd4be2aff22a6";
            string sign = string.Empty;

            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(timesStamp + "#" + app_secret);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                sign = sb.ToString().ToLower();
            }

            string ParameterBuilder = Parameter + "&app_key=" + app_key + "&timestamp=" + timesStamp + "&sign=" + sign;
            
            //dapatkan transform connection domain and replace here.
            var EndPoint = @tcdomain + "/api/v1/guest?" + ParameterBuilder;
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
            {
                return true;
            };
            try
            {
                var multipartContent = new MultipartFormDataContent();
                var fileContent = new StreamContent(keys.OpenReadStream());
                string boundary = "----" + DateTime.Now.Ticks.ToString("x");
                byte[] endBytes = System.Text.Encoding.UTF8.GetBytes("--" + boundary + "--");
                fileContent.Headers.Add("Content-Type", ContentType);
                fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data")
                {
                    Name = "avatarFile",
                    FileName = keys.FileName
                };
                multipartContent.Add(fileContent);

                HttpClient httpClient = new HttpClient(httpClientHandler) { BaseAddress = new Uri(EndPoint) };
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));//ACCEPT header
                var content = await httpClient.PostAsync(EndPoint, multipartContent);

                string result = content.Content.ReadAsStringAsync().Result;
                Console.WriteLine(result);
                return result;
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }
    }
}
