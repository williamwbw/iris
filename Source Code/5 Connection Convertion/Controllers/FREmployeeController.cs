﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using StringBuilder = System.Text.StringBuilder;
using IRISTestConnection.Lib;
using MySqlConnector;
using Microsoft.Extensions.Configuration;

namespace IRISTestConnection.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FREmployeeController : Controller
    {
        public IConfiguration config { get; }
        public FREmployeeController(IConfiguration iConfig)
        {
            config = iConfig;
        }
        [HttpGet]
        public async Task<string> GetAsync(string Data)
        {


            APISetting objSetting = new APISetting(config);
            var keys = Request.Form.Files[0];


            string app_key = objSetting.GetValue("SecuritySettingAppKey", "App_key");
            string app_secret = objSetting.GetValue("SecuritySettingAppSecret", "App_secret");
            string fremployeedomain = objSetting.GetValue("FREmployeeDomain", "domain");
            var client_appkey = new RestClient(app_key);

            //dapatkan app key here
          //  string app_key = "fbdff3fc15d13e2b";
            DateTime now = DateTime.UtcNow;
            long unixTimeMilliseconds = new DateTimeOffset(now).ToUnixTimeMilliseconds();
            string timesStamp = unixTimeMilliseconds.ToString();

            //dapatkan app secret here
           // string app_secret = "dfdaba65c8b29dea850bd4be2aff22a6";
            var client_appsecret = new RestClient(app_secret);
            string sign = string.Empty;

            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {

                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(timesStamp + "#" + app_secret);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                sign = sb.ToString().ToLower();
            }

            string ParameterBuilder = Data;
            var EndPoint = @fremployeedomain + "/api/v1/user?" + ParameterBuilder + "&app_key=" + app_key + "&timestamp=" + timesStamp + "&sign=" + sign;
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
            {
                return true;
            };

            var data = new StringContent("", Encoding.UTF8, "application/json");
            HttpClient httpClient = new HttpClient(httpClientHandler) { BaseAddress = new Uri(EndPoint) };
            var content = await httpClient.PostAsync(EndPoint, data);

            string result = content.Content.ReadAsStringAsync().Result;
            return result;
        }
    }
}
