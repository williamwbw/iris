﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using IRISTestConnection.Lib;
using Microsoft.Extensions.Configuration;
using System.Net;

namespace IRISTestConnection.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FRLogin : Controller
    {
        public IConfiguration config { get; }
        public FRLogin(IConfiguration iConfig)
        {
            config = iConfig;
        }
        [HttpGet]
        public Task<string> GetAsync(string Data)
        { 
            //creating the object from class APISetting
            APISetting objSetting = new APISetting(config);

             string frlogindomain = objSetting.GetValue("FRLoginDomain", "domain");
             string username = objSetting.GetValue("username", "String");
             string password = objSetting.GetValue("password", "String");
             string roles = objSetting.GetValue("roles", "Integer");


            //get the value of FRlogin domain 
            var client = new RestClient(frlogindomain);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //get the value for username, password, roles.
                request.AddParameter("username", username);
                request.AddParameter("password", password);
                request.AddParameter("roles", roles);
           // request.AddParameter("username", "admin");
           // request.AddParameter("password", "123456");
           // request.AddParameter("roles", "4");
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);

            string strtoken = "";
            if (response.StatusCode == HttpStatusCode.OK)
            {
                if(response.Content!=null)
                {
                    JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                    var token = jObject.SelectToken("payload").SelectToken("token");
                    strtoken = token.ToString().Replace("Bearer ", string.Empty);
                }
            }
            
            return Task.FromResult(strtoken);
        }
    }
}
