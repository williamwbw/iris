package com.IRISAPIWD.IRISAPIWD.Controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.Console;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;
import org.jboss.resteasy.util.Base64.InputStream;

import com.IRISAPIWD.IRISAPIWD.Modal.*;

@RestController

public class RESTApi_MiddleMan {

    @RequestMapping("/api/getInformation")
    public boolean getInformation(String Data) throws Exception {
        JSONArray getData = SignData.sign(Data);
        boolean test = verifyInformation(getData.toString());
        //return CommonControl.extract(getData.toString(), "KeyPair").toString();
        //return CommonControl.extract(getData.toString(), "PublicKey").toString();
        //return getData.toString();
        return test;
    }

    @RequestMapping("/api/verifyInformation")
    public boolean verifyInformation(String Data) throws Exception {
        return VerifyData.verifyData(Data);
    }

    @RequestMapping("/api/EncodeData")
    public void doEncodeData(String Data) throws Exception {
        JSONArray getData = Encryption.EncyptionData(Data);
        URL url = new URL("http://192.168.56.102:1233/api/DecodeData");
        System.out.println(url);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setDoOutput(true);
        OutputStream wr = conn.getOutputStream();
        wr.write(getData.toString().getBytes());
        wr.flush();
        wr.close();
        int responseCode = conn.getResponseCode();
        System.out.println(responseCode);
        if(responseCode == HttpURLConnection.HTTP_CREATED){
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while((inputLine = in.readLine()) != null){
                response.append(inputLine);
            }

            in.close();
        }
        conn.disconnect();
        //return CommonControl.extract(getData.toString(), "KeyPair").toString();
        //return CommonControl.extract(getData.toString(), "PublicKey").toString();
        //return getData.toString();
    }

    @PostMapping(value="/api/DecodeData",
    consumes = {MediaType.ALL_VALUE})
    public String doDecode(@RequestBody String JA) throws Exception {
        System.out.println(JA);
        return Descrytion.DecryptionData(JA);
    }
}
