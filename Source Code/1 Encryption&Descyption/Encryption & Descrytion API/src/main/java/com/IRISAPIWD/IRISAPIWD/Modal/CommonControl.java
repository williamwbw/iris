package com.IRISAPIWD.IRISAPIWD.Modal;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import com.github.wnameless.json.flattener.JsonFlattener;

import org.json.JSONArray;
import org.json.JSONObject;

public class CommonControl {

    public static List<String> extract(String json, String key) {

        List<String> urls = new ArrayList<>();

        try {
            JSONArray jsonObject = new JSONArray(json);
            Map<String, Object> flattenedJsonMap = JsonFlattener.flattenAsMap(jsonObject.toString());
            flattenedJsonMap.forEach((k, v) -> {
                if (k.contains(key)) {
                    urls.add(v.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return urls;

    }

    public static String savePublicKey(PublicKey publ) throws GeneralSecurityException {
        KeyFactory fact = KeyFactory.getInstance("DSA");
        X509EncodedKeySpec spec = fact.getKeySpec(publ, X509EncodedKeySpec.class);
        return Base64.getEncoder().encode(spec.getEncoded()).toString();
    }

    public static PublicKey loadPublicKey(String stored) throws GeneralSecurityException, UnsupportedEncodingException {
        byte[] data = stored.getBytes("UTF-8");
        X509EncodedKeySpec spec = new X509EncodedKeySpec(data);
        KeyFactory fact = KeyFactory.getInstance("DSA");
        return fact.generatePublic(spec);
    }
    
}
