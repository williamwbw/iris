package com.IRISAPIWD.IRISAPIWD.Modal;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

public class SignData {
    public static JSONArray sign(String string) throws Exception {
        
        Signature signature = Signature.getInstance("SHA256WithDSA");
        SecureRandom secureRandom = new SecureRandom();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        String PUBLICKEY_PREFIX    = "-----BEGIN PUBLIC KEY-----";
        String PUBLICKEY_POSTFIX   = "-----END PUBLIC KEY-----";
        String PRIVATEKEY_PREFIX   = "-----BEGIN RSA PRIVATE KEY-----";
        String PRIVATEKEY_POSTFIX  = "-----END RSA PRIVATE KEY-----";

         // THIS IS java.security

         PublicKey publicKey = keyPair.getPublic();
         PrivateKey privateKey = keyPair.getPrivate();
         System.out.println("Public key Ori: " + publicKey);
         System.out.println("Public java.security: Algorithm: " + publicKey.getAlgorithm() + "Format: " + publicKey.getFormat());
         System.out.println("Private java.security: Algorithm: " + privateKey.getAlgorithm() + "Format: " + privateKey.getFormat() + "\n");
 
         // THIS IS DER:
 
         byte[] publicKeyDER = publicKey.getEncoded();
         byte[] privateKeyDER = privateKey.getEncoded();
 
         System.out.println("Public DER: "+Arrays.toString(publicKeyDER));
         System.out.println("Private DER: "+Arrays.toString(privateKeyDER) + "\n");
 
         // THIS IS PEM:
 
         String publicKeyPEM = PUBLICKEY_PREFIX + "\n" + DatatypeConverter.printBase64Binary(publicKey.getEncoded()).replaceAll("(.{64})", "$1\n") + "\n" + PUBLICKEY_POSTFIX;
         String privateKeyPEM = PRIVATEKEY_PREFIX + "\n" + DatatypeConverter.printBase64Binary(privateKey.getEncoded()).replaceAll("(.{64})", "$1\n") + "\n" + PRIVATEKEY_POSTFIX;
 
         System.out.println("Public PEM: " + "\n"+publicKeyPEM);
         System.out.println("Private PEM: " + "\n"+privateKeyPEM + "\n");

        signature.initSign(privateKey);
        byte[] data = string.getBytes(StandardCharsets.UTF_8); 
        System.out.println("Data: "+data.toString());
        signature.update(data);

        byte[] digitalSignature = signature.sign();

        String strSignature = "-----BEGIN SIGNATURE-----" + "\n"
                    + DatatypeConverter.printBase64Binary(digitalSignature).replaceAll("(.{64})", "$1\n") + "\n"
                    + "-----END SIGNATURE-----";

        System.out.println("Message Plaintext: " + string);
        System.out.println("Signature: " + "\n" + signature + "\n");
        //String original = "It's a secret that C++ developer are better than Java"; 
        //byte[] bytes = string.getBytes(StandardCharsets.UTF_8); 
        String base64Encoded = Base64.encodeBase64String(data); 
        System.out.println("original text: " + string); 
        System.out.println("Base64 encoded text: " + base64Encoded); 
        // Decode 
        byte[] asBytes = Base64.decodeBase64(base64Encoded); 
        String base64Decoded = new String(asBytes, StandardCharsets.UTF_8); 
        System.out.println("Base64 decoded text: " + base64Decoded);
        //Create JSON to Store Signature Data
        JSONObject SignatureDetails = new JSONObject();
        SignatureDetails.put("SecureRandom",secureRandom.toString());
        SignatureDetails.put("KeyPair",keyPair);
        SignatureDetails.put("Data",base64Encoded);
        SignatureDetails.put("PublicKey",Base64.encodeBase64String(publicKeyDER));
        SignatureDetails.put("Signature",Base64.encodeBase64String(digitalSignature));

        //Put JSON to List
        JSONArray DataList = new JSONArray();
        DataList.put(SignatureDetails);
        return DataList;
	}
}
