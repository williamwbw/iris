package com.IRISAPIWD.IRISAPIWD.Modal;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import org.apache.commons.codec.binary.Base64;

import com.IRISAPIWD.IRISAPIWD.Modal.*;

public class Descrytion {

    public static String DecryptionData(String Data) throws Exception {
        String strData = CommonControl.extract(Data.toString(), "Data").toString();
        byte[] DatabackToBytes = Base64.decodeBase64(strData);
        String strPublicKey = CommonControl.extract(Data.toString(), "PublicKey").toString();
        byte[] PubkeyBackToBytes = Base64.decodeBase64(strPublicKey);
        String strPrivateKey = CommonControl.extract(Data.toString(), "PrivateKey").toString();
        byte[] PrikeyBackToBytes = Base64.decodeBase64(strPrivateKey);
        String strSecretKey = CommonControl.extract(Data.toString(), "SecretKey").toString();
        byte[] ScrkeyBackToBytes = Base64.decodeBase64(strSecretKey);

        PublicKey PK = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(PubkeyBackToBytes));

        //byte[] decryptedKey = decryptKey(PK, PubkeyBackToBytes);
        //SecretKey SK = new SecretKeySpec(ScrkeyBackToBytes, 0, ScrkeyBackToBytes.length, "RSA"); 

        Cipher aesCipher = Cipher.getInstance("RSA");
        aesCipher.init(Cipher.DECRYPT_MODE, PK);
        byte[] bytePlainText = aesCipher.doFinal(DatabackToBytes);
        String plainText = new String(bytePlainText);
        return plainText;
    } 

    public static byte[] decrypt(String data, PublicKey PK) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher aesCipher = Cipher.getInstance("RSA");
        aesCipher.init(Cipher.DECRYPT_MODE, PK);
        byte[] byteCipherText = aesCipher.doFinal(data.getBytes());
        return byteCipherText;
    }

    public static byte[] decryptKey(PrivateKey PK, byte[] EPK) throws InvalidKeyException, NoSuchAlgorithmException,
            NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.PUBLIC_KEY, PK);
        byte[] encryptedKey = cipher.doFinal(EPK/*Seceret Key From Step 1*/);
        return encryptedKey;
    }
}
