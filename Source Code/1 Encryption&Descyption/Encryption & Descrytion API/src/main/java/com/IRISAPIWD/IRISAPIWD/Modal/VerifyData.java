package com.IRISAPIWD.IRISAPIWD.Modal;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;
import org.apache.commons.codec.binary.Base64;


public class VerifyData {
    public static boolean verifyData(String SignedData) throws InvalidKeyException, java.security.SignatureException,
            NoSuchAlgorithmException, InvalidKeySpecException {

        Signature signature = Signature.getInstance("SHA256WithDSA");
        String strPublicKey = CommonControl.extract(SignedData.toString(), "PublicKey").toString();
        byte[] keyBackToBytes = Base64.decodeBase64(strPublicKey);
        //strPublicKey = strPublicKey.replace("-----BEGIN PUBLIC KEY-----","");
        //strPublicKey = strPublicKey.replace("-----END PUBLIC KEY-----","");
        String strSignature = CommonControl.extract(SignedData.toString(), "Signature").toString();
        byte[] backToBytes = Base64.decodeBase64(strSignature);
        String strData = CommonControl.extract(SignedData.toString(), "Data").toString();
        byte[] DatabackToBytes = Base64.decodeBase64(strData);
        //byte[] publicBytes = strPublicKey.getBytes("UTF-8");
        // byte[] decodedCert = Base64.decodeBase64(publicBytes);
        // CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        // InputStream in = new ByteArrayInputStream(decodedCert);
        // X509Certificate certificate = (X509Certificate)certFactory.generateCertificate(in);
        PublicKey pubKey = KeyFactory.getInstance("DSA").generatePublic(new X509EncodedKeySpec(keyBackToBytes));
        System.out.println("Public DER: "+pubKey);
        signature.initVerify(pubKey);
        byte[] asBytes = Base64.decodeBase64(CommonControl.extract(SignedData.toString(), "Data").toString()); 
        String base64Decoded = new String(asBytes); 
        System.out.println("Base64 decoded text out: " + base64Decoded);
        System.out.println("Base64 decoded text out byte: " + asBytes);
        byte[] data = asBytes;
        System.out.println("Data in: "+DatabackToBytes);
        signature.update(DatabackToBytes);

        boolean verified = signature.verify(backToBytes);
        return verified;
    }
}
