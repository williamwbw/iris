package com.IRISAPIWD.IRISAPIWD.Modal;

public class APIPath {
    //Middle Man Client Server API Path
    String MClientPath = "http:/localhost:8080/";
    //Middle Man Host Server API Path
    String MHostPath = "http:/localhost:8080/";

    public String getPath(String MiddleMan){
        try{
            String strGetPath = "";
            switch(MiddleMan.toLowerCase()){
                case "client":
                    strGetPath = MClientPath;
                break;
                case "host":
                    strGetPath = MHostPath;
                break;
            }
            return strGetPath;
        }catch(Exception ex){
            return "Failed To Get Path! Error: " + ex.getMessage();
        }
    }
}
