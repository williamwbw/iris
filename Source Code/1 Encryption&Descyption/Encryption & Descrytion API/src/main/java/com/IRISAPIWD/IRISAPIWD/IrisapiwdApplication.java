package com.IRISAPIWD.IRISAPIWD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.IRISAPIWD.IRISAPIWD.Controller"})
public class IrisapiwdApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrisapiwdApplication.class, args);
	}

}
