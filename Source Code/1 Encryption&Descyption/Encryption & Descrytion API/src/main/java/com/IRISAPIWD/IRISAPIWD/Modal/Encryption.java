package com.IRISAPIWD.IRISAPIWD.Modal;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

import com.IRISAPIWD.IRISAPIWD.Modal.*;

public class Encryption {
    
    public static JSONArray EncyptionData(String string) throws Exception {
        PrivateKey privateKey;
        PublicKey publicKey;

        String ConvertedPublicKey;

        KeyGenerator generator = KeyGenerator.getInstance("AES");
        generator.init(128); // The AES key size in number of bits
        SecretKey secKey = generator.generateKey();
        
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        KeyPair pair = keyGen.generateKeyPair();
        privateKey = pair.getPrivate();
        publicKey = pair.getPublic();

        ConvertedPublicKey = Base64.encodeBase64String(publicKey.getEncoded());

        byte[] publicKeyDER = publicKey.getEncoded();
        byte[] privateKeyDER = privateKey.getEncoded();
        byte[] sceretKeyDER = secKey.getEncoded();

        JSONObject EncyptionDetails = new JSONObject();
        EncyptionDetails.put("KeyPair",publicKey);
        EncyptionDetails.put("Data",Base64.encodeBase64String(encrypt(string, privateKey)));
        EncyptionDetails.put("PublicKey",Base64.encodeBase64String(publicKeyDER));
        EncyptionDetails.put("PrivateKey",Base64.encodeBase64String(encryptPriKey(privateKey, secKey)));
        EncyptionDetails.put("SecretKey",Base64.encodeBase64String(sceretKeyDER));

        //Put JSON to List
        JSONArray DataList = new JSONArray();
        DataList.put(EncyptionDetails);
        return DataList;
    }

    public static byte[] encrypt(String data, PrivateKey PK) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher aesCipher = Cipher.getInstance("RSA");
        aesCipher.init(Cipher.ENCRYPT_MODE, PK);
        byte[] byteCipherText = aesCipher.doFinal(data.getBytes());
        return byteCipherText;
    }

    public static byte[] encryptKey(PublicKey PK, SecretKey SK) throws InvalidKeyException, NoSuchAlgorithmException,
            NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.PUBLIC_KEY, PK);
        byte[] encryptedKey = cipher.doFinal(SK.getEncoded()/*Seceret Key From Step 1*/);
        return encryptedKey;
    }

    public static byte[] encryptPriKey(PrivateKey PK, SecretKey SK) throws InvalidKeyException, NoSuchAlgorithmException,
        NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, PK);
        byte[] encryptedKey = cipher.doFinal(SK.getEncoded()/*Seceret Key From Step 1*/);
        return encryptedKey;
    }
}
