package code.java.UsersREST.CRUD;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import code.java.UsersREST.payload.request.ApiSettingRequest;
import code.java.UsersREST.payload.response.MessageResponse;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/apisetting")
public class ApiSettingController {

    @Autowired
    private ApiSettingService service;

    @Autowired
    private ApiSettingRepository repo;


   @GetMapping("/list")
   public List<ApiSetting> list() {
       return service.listAll();
   }


   @GetMapping("/list/id={id}")
   public ResponseEntity<ApiSetting> get(@PathVariable Long id) {
       try {
           ApiSetting apiSetting = service.get(id);
           return new ResponseEntity<ApiSetting>(apiSetting, HttpStatus.OK);
       } catch (NoSuchElementException e) {
           return new ResponseEntity<ApiSetting>(HttpStatus.NOT_FOUND);
       }
   }

   @PutMapping("/changeapisetting/id={id}")

	public ResponseEntity<?> ChangeApiSetting(@RequestBody ApiSettingRequest ApiSettingRequest, @PathVariable Long id) {

            ApiSetting apiSetting = service.get(id);
            String name = ApiSettingRequest.getSetting_name();
            apiSetting.setSetting_name(ApiSettingRequest.getSetting_name());
            apiSetting.setType(ApiSettingRequest.getType());
            apiSetting.setValue(ApiSettingRequest.getValue());    
            service.update(apiSetting);    
            return ResponseEntity.ok(new MessageResponse("API Settings Updated successfully!" + name));

         
	}

   

    @GetMapping("/testsecret")
    public List<ApiSetting> getSecret() {
        ApiSetting objSetting = new ApiSetting();
        String tcip = objSetting.GetSetting("TransformationConnectionIP", "IP");
        String tcport = objSetting.GetSetting("TransformationConnectionPort", "Port");

        String frip = objSetting.GetSetting("FRLoginIP", "IP");
        String frport = objSetting.GetSetting("FRLoginPort", "Port");

        String dip = objSetting.GetSetting("DecryptIP", "IP");
        String dport = objSetting.GetSetting("DecryptPort", "Port");
        
        return service.getAppSecrett();  
    }

   
   @DeleteMapping("/delete/id={id}")
   public void delete(@PathVariable Long id) {
       service.delete(id);
   }

//    @PostMapping("/addapisetting")
//    public ResponseEntity<?> addApiSetting(@RequestBody ApiSetting apiSetting) {

//        apiSetting.setValue(apiSetting.getValue());
//        service.save(apiSetting);

//        return ResponseEntity.ok(new MessageResponse("OK!"));

//    }

  
   
   

    
	






}