package code.java.UsersREST.CRUD;

import java.util.List;
 
import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import code.java.UsersREST.CRUD.model.Users;
 
@Service
@Transactional
public class UsersService {
 
    @Autowired
    private UsersRepository repo;
     
    
    public List<Users> listAll() {
        return repo.getUsersList();
    }
     
    public void save(Users users) {
        repo.save(users);
    }
     
    public Users get(Integer id) {
        return repo.findById(id).get();
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }
    
    
    public void updateResetPasswordToken(String token, String email) throws Exception {

        Users users = repo.findByEmail(email);
        if (users != null) {
            users.setResetPasswordToken(token);
            repo.save(users);
        } else {
            throw new Exception("Could not find any user with the email " + email);
        }
    }
     
    public Users getByResetPasswordToken(String token) {
        return repo.findByResetPasswordToken(token);
    }
     
   
    public void updatePassword(Users users, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        users.setPassword(encodedPassword);
         
        users.setResetPasswordToken(null);
        repo.save(users);
    }
}
