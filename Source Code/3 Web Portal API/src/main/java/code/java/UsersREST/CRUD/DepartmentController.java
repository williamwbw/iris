
package code.java.UsersREST.CRUD;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/department")
public class DepartmentController {

    @Autowired
    private DepartmentService service;

    @Autowired
    private DepartmentRepository repo;

    @GetMapping("/list")
    public List<Department> list() {
        return repo.getDepartmentList();
    }

    @GetMapping("/list/id={id}")
    public ResponseEntity<Department> get(@PathVariable Integer id) {
        try {
            Department department = service.get(id);
            return new ResponseEntity<Department>(department, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public void add(@RequestBody Department department) {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        department.setcreatedatetime(formatter.format(date));
        department.setmodifydatetime(formatter.format(date));
        service.save(department);
    }

    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody Department department, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            Department existDepartment = service.get(id);
            existDepartment.setDepartmentname(department.getDepartmentname());
            existDepartment.setStatus(department.getstatus());
            existDepartment.setmodifyuserid(department.getmodifyuserid());
            existDepartment.setmodifydatetime(formatter.format(date));
            service.save(existDepartment);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/department/id={id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
} 
