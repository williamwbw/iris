package code.java.UsersREST.CRUD;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/visitor")
public class VisitorsController {
    
    @Autowired
    private VisitorsService service;

    @Autowired
    private VisitorsRepository repo;

    @Autowired
    private WFTransactionController wfTransaction;

    @Autowired
    private BlacklistsController blackList;


   @GetMapping("/list")
   public List<Visitors> list() {
       return service.listAll();
   }


   @GetMapping("/report")
    public List<Object> report() {
        return repo.getCheckInOutReport();
    }


    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/list/{id}")
    public ResponseEntity<Visitors> get(@PathVariable Integer id) {
        try {
            Visitors visitors = service.get(id);
            return new ResponseEntity<Visitors>(visitors, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Visitors>(HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @PostMapping("/add")
    public Boolean add(@RequestBody Visitors visitors) throws IOException {

        List<Blacklists> lsBlackList = blackList.list();
        Boolean valid = false;
        Boolean found = false;
        for (Blacklists blacklists : lsBlackList) {
            blacklists.getName();
            visitors.getName();
            // Boolean test1 = blacklists.getName().equals(visitors.getName());
            // Boolean test2 = blacklists.getICPassport().equals(visitors.geticnumber());

            if (blacklists.getName().equals(visitors.getName())
                    || blacklists.getICPassport().equals(visitors.geticnumber())) {
                valid = checkValidDate(blacklists.getFromDate(), blacklists.getToDate(), visitors.getFromDate());

                if (!valid) {
                    return valid;
                } else {
                    addVisitor(visitors);
                }
                found = true;
            }
        }
        if (!found) {
            addVisitor(visitors);
            return true;
        }
        return valid;
    }
    
    private void addVisitor(Visitors visitors) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        visitors.setTimeStamp(formatter.format(date));
        visitors.setcreatedatetime(formatter.format(date));
        visitors.setmodifydatetime(formatter.format(date));
        visitors.setStatus("Pending");
        visitors.setRemarks("Pending");
        visitors.setGroupDefault(0);
        visitors.setcreateuserid(1);
        visitors.setmodifyuserid(1);
        visitors.setReceptionUserID(1);
        service.save(visitors);

        Visitors SavedVisitor = service.save(visitors);
        int VID = SavedVisitor.getid();

        wfTransaction.add(VID);


    }
    private Boolean checkValidDate(String fDate, String tDate, String kDate) {
        try {
            Date fr_Date = new SimpleDateFormat("yyyy-MM-dd").parse(fDate);
            Date to_Date = new SimpleDateFormat("yyyy-MM-dd").parse(tDate);
            Date ky_Date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(kDate);

            if (ky_Date.after(fr_Date) && ky_Date.before(to_Date)) {
                return false;
            }else
            {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }


    // @PostMapping("/addFR")
    @RequestMapping(value="/addFR",
                method=RequestMethod.POST)
    public String addFR(@RequestParam("avatarFile") MultipartFile sourceFile, String name, String mobile, 
                        String remark, String groups, String receptionUserId, String dateTimeFrom, String dateTimeTo, 
                        String mail, String position, String gender ,String guestCompany, String guestsPurpose, String icNumber, 
                        String Prompt, String areaCode, String idNumber){
        try {
            Boolean test = true;
            
            if(test){
            //first, dapatkan app_key and app_secret from db here
            String testData ="name=jamesbond&mobile=0123456789&remark=test&groups=1&receptionUserId=1&dateTimeFrom=2021-07-01 12:00:00&dateTimeTo=2021-"+
                    "07-02 17:00:00&mail=jamesbond@iris.com.my&position=null&gender=1&guestCompany=testing&guestsPurpose=null&icNumber=123456&prompt=test&areaCode=012&idNumber=111111";
            // byte[] something = Utility.signData(testData);
            // StringBuilder sb = new StringBuilder();
            // for(byte b: something){
            //     sb.append(String.format("%02X",b));
            // }

            // String testsign = sb.toString();
            return "0";
        } else {
            ApiSetting objSetting = new ApiSetting();
            String app_key = objSetting.GetSetting("SecuritySettingAppKey", "App_Key");
            String app_secret = objSetting.GetSetting("SecuritySettingAppSecret", "App_Secret");
            String timesStamp = String.valueOf(Instant.now().toEpochMilli());

            //HASH with fomula md5(timestamp#app_secret)
            String sign = getMd5(timesStamp + "#" + app_secret);

            // //Save into Portal
            Visitors VSC = new Visitors();
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            VSC.setTimeStamp(formatter.format(date));
            VSC.setName(name);
            VSC.seticnumber(icNumber);
            VSC.setGender(gender);
            VSC.setContactNumber(mobile);
            VSC.setEmail(mail);
            VSC.setFromDate(dateTimeFrom);
            VSC.setToDate(dateTimeTo);
            VSC.setGroupDefault(0);
            VSC.setGuestCompany(guestCompany);
            VSC.setReceptionUserID(1);
            VSC.setRemarks(remark);
            VSC.setVisitPurpose(guestsPurpose);
            VSC.setVisitDepartment("1");
            VSC.setStatus("Pending");
            VSC.setcreatedatetime(formatter.format(date));
            VSC.setcreateuserid(1);
            VSC.setmodifydatetime(formatter.format(date));
            VSC.setmodifyuserid(1);

            Visitors SavedVisitor = service.save(VSC);
            int VID = SavedVisitor.getid();

            // //second, dapatkan IP and PORT from WorkFlow management

            // String wfip = objSetting.GetSetting("WorkflowManagementIP", "IP");
            // String wfport = objSetting.GetSetting("WorkflowManagementPort", "PORT");

            // // //Redirect to workflow API
            // URL URL = new URL("http://"+ wfip +":"+ wfport +"/wftransaction/add/visitorid=" + VID);
            // HttpURLConnection con = (HttpURLConnection)URL.openConnection();

            // System.out.println(URL);
            //  print_content(con);
            // WFTransactionController wfTransaction = new WFTransactionController();
            wfTransaction.add(VID);

            String ParameterBuilder = "name=" + name + "&mobile=" + mobile + "&remark=" + remark + "&groups=" + groups
                    + "&receptionUserId=" + receptionUserId + "&dateTimeFrom=" + dateTimeFrom + "&dateTimeTo="
                    + dateTimeTo + "&mail=" + mail + "&position=" + position + "&gender=" + gender + "&guestCompany="
                    + guestCompany + "&guestsPurpose=" + guestsPurpose + "&icNumber=" + icNumber + "&prompt=" + Prompt
                    + "&areaCode=" + areaCode + "&idNumber=" + idNumber;
            System.out.println(ParameterBuilder);
            Redirection(ParameterBuilder, sourceFile, VID);

            return "app_key = " + app_key + "\r\ntimeStamp = " + timesStamp + "\r\nsign = " + sign
                    + "\r\nCurrentTime = " + Instant.now() + "\r\nResult = ";
        }
        }catch(Exception ex){
            return ex.getMessage();
        }
    }

    private int ParseInt(String getSetting) {
        return 0;
    }

    public String Redirection(String Parameter, MultipartFile sourceFile, Integer VID) throws IOException, NoSuchAlgorithmException, KeyManagementException{


        // upload image into resources

        byte[] bytes = sourceFile.getBytes();
        String fileType = sourceFile.getOriginalFilename();
        String[] parts = fileType.split(Pattern.quote("."));
        String part1 = parts[0]; 
        String part2 = parts[1];

        String pathRest = new File("").getAbsolutePath();
        File pathRest2 = new File(pathRest);
        File pathVue = new File(pathRest2, "../2 Web Portal/public/upload/" + VID + "." + part2);
        String absolute = pathVue.getCanonicalPath();
        System.out.println(pathRest);
        System.out.println(pathRest2);
        System.out.println(pathVue);
        System.out.println(absolute);
        Path path = Paths.get(absolute);
        
        Files.write(path, bytes);
            
    
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };
        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }

        // Now you can access an https URL without having the certificate in the truststore
        try {
            //third, dapatkan the ip & port from the setting name
            ApiSetting objSetting = new ApiSetting();
            String tcip = objSetting.GetSetting("TransformationConnectionIP", "IP");
            String tcport = objSetting.GetSetting("TransformationConnectionPort", "PORT");

            URL URL = new URL("https://" + tcip + ":" + tcport + "/TransformConnection?ContentType="+sourceFile.getContentType()+"&Parameter="+ URLEncoder.encode(Parameter, "UTF-8"));
            System.out.println(URL);
            HttpsURLConnection con = (HttpsURLConnection)URL.openConnection();
            con.setDoOutput(true);
            String boundary = UUID.randomUUID().toString();
            byte[] boundaryBytes = 
                       ("--" + boundary + "\r\n").getBytes(StandardCharsets.UTF_8);
            byte[] finishBoundaryBytes = 
                       ("--" + boundary + "--").getBytes(StandardCharsets.UTF_8);
                       con.setRequestProperty("Content-Type", 
                       "multipart/form-data; charset=UTF-8; boundary=" + boundary);
            
            // Enable streaming mode with default settings
            con.setChunkedStreamingMode(0); 
            
            // Send our fields:
            try(OutputStream out = con.getOutputStream()) {
                // Send our header (thx Algoman)
                out.write(boundaryBytes);
            
                // Send our first field
                sendField(out, "avatarFile", sourceFile.getOriginalFilename());
            
                // Send another seperator
                out.write(boundaryBytes);

                
            
                // // Send our file
                // try(InputStream inputstream = sourceFile.getInputStream()) {
                //     //sendFile(out, sourceFile.getOriginalFilename(), file, sourceFile.getOriginalFilename()); 
                    
                //     // System.out.println(sourceFile.getOriginalFilename());
                   
           
                // }
                
               
            
                // Finish the request
                out.write(finishBoundaryBytes);
            }catch(Exception ex){
                System.out.print(ex.getMessage());
            }

        

            // con.setRequestMethod("POST");
            // con.setRequestProperty("Content-Type", "multipart/form-data;");
            // con.setDoOutput(true);

            // System.out.println(sourceFile.getBytes());

            // try(OutputStream os = con.getOutputStream()) {
            //     byte[] input = sourceFile.getBytes();
            //     os.write(input, 0, input.length);			
            // }catch(Exception ex){
            //     System.out.println(ex.getMessage().toString());
            // }

            // try(BufferedReader br = new BufferedReader(
            //     new InputStreamReader(con.getInputStream(), "utf-8"))) {
            //         StringBuilder response = new StringBuilder();
            //         String responseLine = null;
            //         while ((responseLine = br.readLine()) != null) {
            //             response.append(responseLine.trim());
            //         }
            //         System.out.println(response.toString());
            //     }catch(Exception ex){
            //         System.out.println(ex.getMessage().toString());
            //     }
            
            // //dumpl all cert info
            // print_https_cert(con);
               
            // //dump all the content
            // print_content(con);

            return con.toString();
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
        
    }

   

    // private void sendFile(OutputStream out, String name, InputStream in, String fileName) throws IOException {
    //     String o = "Content-Disposition: form-data; name=\"" + URLEncoder.encode(name,"UTF-8") 
    //              + "\"; filename=\"" + URLEncoder.encode(fileName,"UTF-8") + "\"\r\n\r\n";
    //     out.write(o.getBytes(StandardCharsets.UTF_8));
    //     byte[] buffer = new byte[2048];
    //     for (int n = 0; n >= 0; n = in.read(buffer))
    //         out.write(buffer, 0, n);
    //     out.write("\r\n".getBytes(StandardCharsets.UTF_8));
    // }

    private void sendField(OutputStream out, String name, String field) throws IOException {
        String o = "Content-Disposition: form-data; name=\"" 
                 + URLEncoder.encode(name,"UTF-8") + "\"\r\n\r\n";
        out.write(o.getBytes(StandardCharsets.UTF_8));
        out.write(URLEncoder.encode(field,"UTF-8").getBytes(StandardCharsets.UTF_8));
        out.write("\r\n".getBytes(StandardCharsets.UTF_8));
    }

    private void print_https_cert(HttpURLConnection con){
     
        if(con!=null){
                
          try {
                    
            System.out.println("Response Code : " + con.getResponseCode());
            // System.out.println("Cipher Suite : " + con.getCipherSuite());
            System.out.println("\n");
                        
            // java.security.cert.Certificate[] certs = con.getServerCertificates();
            // for(java.security.cert.Certificate cert : certs){
            // System.out.println("Cert Type : " + cert.getType());
            // System.out.println("Cert Hash Code : " + cert.hashCode());
            // System.out.println("Cert Public Key Algorithm : " 
            //                                 + cert.getPublicKey().getAlgorithm());
            // System.out.println("Cert Public Key Format : " 
            //                                 + cert.getPublicKey().getFormat());
            // System.out.println("\n");
            // }
                        
            } catch (SSLPeerUnverifiedException e) {
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }
    
         }
        
       }
        
       private void print_content(HttpURLConnection con){
        if(con!=null){
                
        try {
            
           System.out.println("****** Content of the URL ********");			
           BufferedReader br = 
            new BufferedReader(
                new InputStreamReader(con.getInputStream()));
                    
           String input;
                    
           while ((input = br.readLine()) != null){
              System.out.println(input);
           }
           br.close();
                    
        } catch (IOException e) {
           e.printStackTrace();
        }
                
           }
            
       }

       public String getMd5(String input) {

           MessageDigest md;
           try {
               md = MessageDigest.getInstance("MD5");
               // digest() method is called to calculate message digest
               //  of an input digest() return array of byte
               byte[] messageDigest = md.digest(input.getBytes());

               // Convert byte array into signum representation
               BigInteger no = new BigInteger(1, messageDigest);

               // Convert message digest into hex value
               String hashtext = no.toString(16);
               while (hashtext.length() < 32) {
                   hashtext = "0" + hashtext;
               }
               return hashtext;
           } catch (NoSuchAlgorithmException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
               return e.getMessage();
           }

       }

    @CrossOrigin(origins = "http://localhost:8080")
    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody Visitors visitors, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            Visitors existVisitors = service.get(id);
            existVisitors.setStatus(visitors.getStatus());
            existVisitors.setmodifyuserid(visitors.getmodifyuserid());
            existVisitors.setmodifydatetime(formatter.format(date));
            service.save(existVisitors);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }

    



}
