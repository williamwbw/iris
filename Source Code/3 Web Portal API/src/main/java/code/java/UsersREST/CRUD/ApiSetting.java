package code.java.UsersREST.CRUD;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Table(name = "api_setting")
public class ApiSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    // @Autowired
    // private ApiSettingService service;

    // @Autowired
    // private ApiSettingRepository repo;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "setting_name", length = 100)
    private String setting_name;

    @Column(name = "type", length = 100)
    private String type;

    @Column(name = "value", length = 100)
    private String value;

    public ApiSetting() {
    }

    public ApiSetting(Long id, String setting_name, String type, String value) {
        this.id = id;
        this.setting_name = setting_name;
        this.type = type;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSetting_name() {
        return setting_name;

    }

    public void setSetting_name(String settingName) {
        this.setting_name = settingName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String GetSetting(String setting_name, String type) {

        String url = "jdbc:mysql://localhost:3306/iris_middleman?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String username = "root";
        String password = "toor";
        String value = "";

        System.out.println("Connecting database...");

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            System.out.println("Database connected!");
            // Connection conn;
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(
                    "select * from api_setting where setting_name = '" + setting_name + "' and type ='" + type + "'");
            while (rs.next()) {
                System.out.println(
                        rs.getString("setting_name") + "  " + rs.getString("type") + "  " + rs.getString("value"));
                value = rs.getString("value");
            }
            conn.close();
        } catch (Exception e) {
            throw new IllegalStateException("Cannot connect the database!", e);
        }
        return value;

    }

}
