package code.java.UsersREST.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import code.java.UsersREST.CRUD.model.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {
    
        @Query("SELECT c FROM Users c WHERE c.email = ?1")
        public Users findByEmail(String email); 
        
        public Users findByResetPasswordToken(String token);

        // return User when status active
        @Query(value = "select * from users where status = 'Active' ", nativeQuery = true)
        public List<Users> getUsersList();
    
       //use for adding 
       @Query(value="select JSON_OBJECT('id',id,'employeename',employeename) from view_crud_getempl_positionrole",nativeQuery = true)
        public List<Object> getAllEmployee();
}