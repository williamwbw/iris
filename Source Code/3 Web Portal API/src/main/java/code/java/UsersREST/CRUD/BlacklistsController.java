package code.java.UsersREST.CRUD;

import java.util.List;
import java.util.NoSuchElementException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/blacklist")
public class BlacklistsController {
    @Autowired
    private BlacklistsService service;

    @Autowired
    private BlacklistsRepository repo;

    @GetMapping("/list")
    public List<Blacklists> list() {
        return repo.getBlackList();
    }

    @GetMapping("/report")
    public List<Object> report() {
        return repo.getAllBlacklistReport();
    }

    @GetMapping("/list/id={}")
    public ResponseEntity<Blacklists> get(@PathVariable Integer id) {
        try {
            Blacklists blacklists = service.get(id);
            return new ResponseEntity<Blacklists>(blacklists, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Blacklists>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public void add(@RequestBody Blacklists blacklists) {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        blacklists.setDateTime(formatter.format(date));
        blacklists.setcreatedatetime(formatter.format(date));
        blacklists.setmodifydatetime(formatter.format(date));
        service.save(blacklists);
    }

    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody Blacklists blacklists, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            Blacklists existBlacklists = service.get(id);
            existBlacklists.setName(blacklists.getName());
            existBlacklists.setICPassport(blacklists.getICPassport());
            existBlacklists.setReason(blacklists.getReason());
            existBlacklists.setFromDate(blacklists.getFromDate());
            existBlacklists.setToDate(blacklists.getToDate());
            existBlacklists.setStatus(blacklists.getstatus());
            existBlacklists.setmodifyuserid(blacklists.getmodifyuserid());
            existBlacklists.setmodifydatetime(formatter.format(date));
            service.save(existBlacklists);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}
