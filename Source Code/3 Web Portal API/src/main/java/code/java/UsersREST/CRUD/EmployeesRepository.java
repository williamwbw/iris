package code.java.UsersREST.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
 
public interface EmployeesRepository extends JpaRepository<Employees, Integer> {

    @Query(value = "SELECT JSON_OBJECT('id',id, 'emplonumber',emplonumber, 'username',username, 'name',name, 'contactnumber',contactnumber, 'email' ,email, 'department',department, 'position',position, 'role',role) FROM employees where status='active'", nativeQuery = true)
    public List<Object> getAllEmployee();
 
}