package code.java.UsersREST.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface VisitorsRepository extends JpaRepository<Visitors, Integer> {

    // return Visitor Check In Out List when status active
    @Query(value = "select JSON_OBJECT('id',v.id ,'name', v.name, 'icnumber', v.icnumber,'time_stamp', v.time_stamp, 'from_date',v.from_date, 'to_date',v.to_date, 'status',v.status, 'purposoname', po.purposename ,'departmentname', de.departmentname, 'imgpro', '') FROM visitors  v left join purpose po on po.id  = v.visit_purpose left join department de on de.id = v.visit_department", nativeQuery = true)
    public List<Object> getVisitors();

    @Query(value="select JSON_OBJECT('id',ROW_NUMBER() OVER (),'name',name,'icnumber',icnumber,'timein',timein,'timeout',timeout,'departmentname',departmentname,'purposename',purposename,'intimestamp',intimestamp,'outtimestamp',outtimestamp) from view_report_checkinout",nativeQuery = true)
    public List<Object> getCheckInOutReport();

    @Query(value = "SELECT * FROM visitors WHERE icnumber = ?1", nativeQuery = true)
    public Visitors findByIcnumber(String icnumber);

    
}
