package code.java.UsersREST.CRUD;

import java.util.List;
 
import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class VisitorsService {
    
    @Autowired
    private VisitorsRepository repo;
     
    public List<Visitors> listAll() {
        return repo.findAll();
    }
     
    public Visitors save(Visitors visitors) {
        return repo.save(visitors);
    }
     
    public Visitors get(Integer id) {
        return repo.findById(id).get();
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }

    
}
