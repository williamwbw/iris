package code.java.UsersREST.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmplRoleRepository extends JpaRepository<EmplRole, Integer> {

     // return Role when status active
     @Query(value = "select * from empl_role where status = 'active' ", nativeQuery = true)
     public List<EmplRole> getRoleList();
}