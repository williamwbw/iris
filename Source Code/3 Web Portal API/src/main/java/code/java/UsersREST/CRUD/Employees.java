package code.java.UsersREST.CRUD;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employees {
    private Integer id;
    private String emplonumber;
    private String username;
    private String name;
    private String contactnumber;
    private String email;
    private String department;
    private String position;
    private String status;
    private Integer createuserid;
    private String createdatetime;
    private Integer modifyuserid;
    private String modifydatetime;
    private String role;

    public Employees() {
    }

    public Employees(Integer id, String emplonumber, String username, String name, String contactnumber, String email, String department, String position, String status,
    Integer createuserid, String createdatetime, Integer modifyuserid, String modifydatetime, String role) {
        this.id = id;
        this.emplonumber = emplonumber;
        this.username = username;
        this.name = name;
        this.contactnumber = contactnumber;
        this.email = email;
        this.department = department;
        this.position = position;
        this.status = status;
        this.createuserid = createuserid;
        this.createdatetime = createdatetime;
        this.modifyuserid = modifyuserid;
        this.modifydatetime = modifydatetime;
        this.role = role;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getemplonumber() {
        return emplonumber;
    }

    public void setemplonumber(String emplonumber) {
        this.emplonumber = emplonumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getcontactnumber() {
        return contactnumber;
    }

    public void setcontactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }

    public String getemail() {
        return email;
    }

    public void setemail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getstatus() {
        return status;
    }

    public String getmodifydatetime() {
        return modifydatetime;
    }

    public String getcreatedatetime() {
        return createdatetime;
    }

    public Integer getmodifyuserid() {
        return modifyuserid;
    }

    public Integer getcreateuserid() {
        return createuserid;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setmodifyuserid(Integer modifyuserid) {
        this.modifyuserid = modifyuserid;
    }

    public void setcreateuserid(Integer createuserid) {
        this.createuserid = createuserid;
    }

    public void setmodifydatetime(String modifydatetime) {
        this.modifydatetime = modifydatetime;
    }

    public void setcreatedatetime(String createdatetime) {
        this.createdatetime = createdatetime;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
