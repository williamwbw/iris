package code.java.UsersREST.CRUD;

import org.json.JSONArray;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@RestController
public class WriteDataController {

    @PostMapping(value = "/api/visitorsData", consumes = { MediaType.ALL_VALUE })
    public String googleFormData(@RequestBody String JA) {
        // get the function to run data inside JA(google form data)
        String visitorID = WriteData.getdata(JA);

        
        // ApiSetting objSetting = new ApiSetting();
        // String wfip = objSetting.GetSetting("WorkflowManagement", "IP");
        // String wfport = objSetting.GetSetting("WorkflowManagement", "PORT");

        // // //Redirect to workflow API
        // // URL URL = new URL("http://localhost:1236/wftransaction/add/visitorid=" + VID);
        // APIConnection.InternalAPI("http://"+ wfip +":"+ wfport +"/wftransaction/add", visitorID);

        APIConnection.InternalAPI("http://localhost:1236/wftransaction/add", visitorID);
        return "Successful";
    }

}
