package code.java.UsersREST.models;

public enum ERole {
	USER,
    ADMIN
}
