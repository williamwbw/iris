package code.java.UsersREST.CRUD;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import code.java.UsersREST.WorkFlowModel.WFSetup;

@Service
@Transactional
public class WFSetupService {

    @Autowired
    private WFSetupRepository repo;

    public List<WFSetup> listAll() {
        return repo.findAll();
    }

    public Object save(WFSetup workflow_setup) {
        Object savedID = repo.save(workflow_setup);
        return savedID;
       
    }

    public WFSetup get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }

    public Optional<WFSetup> findById(Integer id) {
        return repo.findById(id);
    }
    

}
