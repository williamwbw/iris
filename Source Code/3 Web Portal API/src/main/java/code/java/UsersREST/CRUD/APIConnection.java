package code.java.UsersREST.CRUD;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class APIConnection {
    public static boolean InternalAPI(String URL, Object Data) {
        try{
        URL url = new URL(URL);
        System.out.println(url);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setDoOutput(true);
        OutputStream wr = conn.getOutputStream();
        wr.write(((String) Data).getBytes());
        wr.flush();
        wr.close();
        int responseCode = conn.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_CREATED) {
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();
        }
        conn.disconnect();
        return true;
    }catch(Exception ex){
        return false;
    }
    }
}
