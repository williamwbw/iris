package code.java.UsersREST.CRUD;

import java.util.List;
import java.util.NoSuchElementException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.crypto.Data;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import org.hibernate.result.Output;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/employees")
public class EmployeesController {

    @Autowired
    private EmployeesService service;

    @Autowired
    private EmployeesRepository repo;



    // @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/list")
    public List<Object> list() {
        return repo.getAllEmployee();
    }

    // @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/list/{id}")
    public ResponseEntity<Employees> get(@PathVariable Integer id) {
        try {
            Employees employees = service.get(id);
            return new ResponseEntity<Employees>(employees, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Employees>(HttpStatus.NOT_FOUND);
        }
    }

    // @CrossOrigin(origins = "http://localhost:8080")
    @PostMapping("/add")
    public void add(@RequestBody Employees employees) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        employees.setcreatedatetime(formatter.format(date));
        employees.setmodifydatetime(formatter.format(date));
        service.save(employees);
    }

    public String Redirection(String Parameter) throws IOException, NoSuchAlgorithmException, KeyManagementException{
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };
        // Install the all-trusting trust manager
        try {
                SSLContext sc = SSLContext.getInstance("TLS");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }

        // Now you can access an https URL without having the certificate in the truststore
        try {
            String Result = "";

            //create an object for ApiSetting class
            ApiSetting objSetting = new ApiSetting();

            //get the frlogin ip and frlogin port
            String frip = objSetting.GetSetting("FRLoginIP", "IP");
            String frport = objSetting.GetSetting("FRLoginPort", "PORT");  
        
            //combine the variables and pass it into the url
            URL URL = new URL("https://"+ frip +":"+ frport +"/FRLogin");
            HttpsURLConnection con = (HttpsURLConnection)URL.openConnection();
            
            // dumpl all cert info
             print_https_cert(con);
               
            // dump all the content
             String Token = print_content(con);
 
            //getting the AMS IP & AMS PORT       
            String amsip = objSetting.GetSetting("AMSEmployeeIP", "IP");
            String amsport = objSetting.GetSetting("AMSEmployeePort", "PORT"); 
       
            URL = new URL("https://"+ amsip +":"+ amsport +"/AMSEmployee?Token=" + Token);
          
            System.out.println(URL);
            HttpsURLConnection conAms = (HttpsURLConnection)URL.openConnection();
            conAms.setDoOutput(true);
            conAms.setRequestMethod("POST");
            conAms.setRequestProperty("Content-Type", "application/json; utf-8");
            try(OutputStream os = conAms.getOutputStream()) {
                byte[] input = Parameter.getBytes("utf-8");
                os.write(input, 0, input.length);			
            }

            try(BufferedReader br = new BufferedReader(
                new InputStreamReader(conAms.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    System.out.println(response.toString());
                    Result = "\r\n***AMS Request & Response***\r\n" + response.toString();
                }

                

            // dumpl all cert info
            //print_https_cert(conAms);
    
            // dump all the content
            //String result = "\r\n***AMS Request & Response***\r\n" + print_content(conAms);

            String fremployeeip = objSetting.GetSetting("FREmployeeIP", "IP");
            String fremployeeport = objSetting.GetSetting("FREmployeePort", "PORT");
            String groups = objSetting.GetSetting("groups", "Visitor");
            String areaCode = objSetting.GetSetting("areaCode", "Visitor"); 
            String prompt = objSetting.GetSetting("prompt", "Visitor"); 
            System.out.println(groups);
            System.out.println(areaCode);
            System.out.println(prompt);
            JSONObject obj = new JSONObject(Parameter);
            System.out.println(obj);
            //String ParameterURL = "name="+ obj.getString("firstName") + " " + obj.getString("surName") +"&icNumber=" + obj.getString("employeeId") + "&mobile=" + obj.getString("contactNumber") + "&remark=null&idNumber=" + obj.getString("employeeId") + "&groups=6&mail=" + obj.getString("email") + "&position=" + obj.getInt("designation") + "&jobNumber=" + obj.getString("employeeId") + "&departmentId=" + obj.getInt("department") + "&areaCode=&location=" + obj.getString("address") + "&gender=1&prompt=null";

            String ParameterURL = "name="+ obj.getJSONObject("data").getString("name") + "&icNumber="+ obj.getJSONObject("data").getString("icNumber") + "&mobile="+ obj.getJSONObject("data").getString("mobile") + "&remark=null&idNumber=" + obj.getJSONObject("data").getString("idNumber") + "&groups=" +groups+ "&jobNumber=" +  obj.getJSONObject("data").getString("jobNumber") +  "&areaCode=" + areaCode + "&location=" +  obj.getJSONObject("data").getString("location") + "&gender=1&prompt=" +prompt;
            URL = new URL("https://"+ fremployeeip +":" + fremployeeport + "/FREmployee?Data=" + URLEncoder.encode(ParameterURL, "UTF-8"));  
            con = (HttpsURLConnection)URL.openConnection();  

            // dumpl all cert info
            print_https_cert(con);

            // dump all the content
            Result += "\r\n***FR Request & Response***\r\n" + print_content(con);

            return Result;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
        
    }
    public String getMd5(String input){
        
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
        return hashtext;
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return e.getMessage();
        }

    }

    private void print_https_cert(HttpsURLConnection con){
     
        if(con!=null){
                
          try {
                    
            System.out.println("Response Code : " + con.getResponseCode());
            System.out.println("Cipher Suite : " + con.getCipherSuite());
            System.out.println("\n");
                        
            java.security.cert.Certificate[] certs = con.getServerCertificates();
            for(java.security.cert.Certificate cert : certs){
            System.out.println("Cert Type : " + cert.getType());
            System.out.println("Cert Hash Code : " + cert.hashCode());
            System.out.println("Cert Public Key Algorithm : " 
                                            + cert.getPublicKey().getAlgorithm());
            System.out.println("Cert Public Key Format : " 
                                            + cert.getPublicKey().getFormat());
            System.out.println("\n");
            }
                        
            } catch (SSLPeerUnverifiedException e) {
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }
    
         }
        
       }
        
       private String print_content(HttpsURLConnection con){
        String Output = "";
        if(con!=null){
                
        try {
            
            System.out.println("****** Content of the URL ********");			
            BufferedReader br = 
                new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
                        
            String input;
            
                        
            while ((input = br.readLine()) != null){
                System.out.println(input);
                Output = input;
            }
            br.close();
                        
            } catch (IOException e) {
                e.printStackTrace();
                return e.getMessage();
            }
                
           }  
           return Output;          
       }

    @PostMapping("/addFR")
    public String addFR(@RequestBody String Data){
        try{
            String Result = Redirection(Data);
            return Result;
        }catch(Exception ex){
            return ex.getMessage();
        }
    }

    // @CrossOrigin(origins = "http://localhost:8080")
    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody Employees employees, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            Employees existEmployees = service.get(id);
            existEmployees.setemplonumber(employees.getemplonumber());
            existEmployees.setName(employees.getName());
            existEmployees.setUsername(employees.getUsername());
            existEmployees.setcontactnumber(employees.getcontactnumber());
            existEmployees.setemail(employees.getemail());
            existEmployees.setDepartment(employees.getDepartment());
            existEmployees.setRole(employees.getRole());
            existEmployees.setPosition(employees.getPosition());
            existEmployees.setStatus(employees.getstatus());
            existEmployees.setmodifyuserid(employees.getmodifyuserid());
            existEmployees.setmodifydatetime(formatter.format(date));
            service.save(existEmployees);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/updateDelete/id={id}")
    public ResponseEntity<?> updateDelete(@RequestBody Employees employees, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            Employees existEmployees = service.get(id);
            existEmployees.setStatus(employees.getstatus());
            existEmployees.setmodifyuserid(employees.getmodifyuserid());
            existEmployees.setmodifydatetime(formatter.format(date));
            service.save(existEmployees);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // @CrossOrigin(origins = "http://localhost:8080")
    @DeleteMapping("/delete/id={id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}