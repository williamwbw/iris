
package code.java.UsersREST.CRUD;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/purpose")
public class PurposeController {

    @Autowired
    private PurposeService service;

    @Autowired
    private PurposeRepository repo;

    @GetMapping("/list")
    public List<Purpose> list() {
        return repo.getPurposeList();
    }

    @GetMapping("/list/id={id}")
    public ResponseEntity<Purpose> get(@PathVariable Integer id) {
        try {
           Purpose purpose = service.get(id);
            return new ResponseEntity<Purpose>(purpose, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Purpose>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public void add(@RequestBody Purpose purpose) {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        purpose.setcreatedatetime(formatter.format(date));
        purpose.setmodifydatetime(formatter.format(date));
        service.save(purpose);
    }

    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody Purpose purpose, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            Purpose existPurpose = service.get(id);
            existPurpose.setPurposename(purpose.getPurposename());
            existPurpose.setStatus(purpose.getstatus());
            existPurpose.setmodifyuserid(purpose.getmodifyuserid());
            existPurpose.setmodifydatetime(formatter.format(date));
            service.save(existPurpose);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/deletepurpose/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
} 
