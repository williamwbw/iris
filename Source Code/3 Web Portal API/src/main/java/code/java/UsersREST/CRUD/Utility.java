package code.java.UsersREST.CRUD;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.time.Instant;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;



public class Utility {
    
    private final static OkHttpClient client = new OkHttpClient();

    public static String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }

    public static String getMd5(String timestamp) {
        ApiSetting objSetting = new ApiSetting();
        String app_key = objSetting.GetSetting("SecuritySettingAppKey", "App_Key");
        String app_secret = objSetting.GetSetting("SecuritySetting", "App_Secret");
        String strToHash = timestamp +"#"+ app_secret;
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(strToHash.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return e.getMessage();
        }

    }
       
    public static String getSign(String timestamp) {

        ApiSetting objSetting = new ApiSetting();
        String app_key = objSetting.GetSetting("SecuritySettingAppKey", "App_Key");
        String app_secret = objSetting.GetSetting("SecuritySetting", "App_Secret");

        //! IMPORTANT
        //! signature for FRServer(Face Recognition Server)
        //! md5(timestamp#appSecret)

        try {
            String strToHash = timestamp + "#" + app_secret;
            byte[] bMessage = strToHash.getBytes();

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] bSignature = md.digest(bMessage);

            StringBuilder sb = new StringBuilder();
            // for(byte b: bSignature){
            //     sb.append(String.format(("%20X"),b));
            // }

            // String strHashedSign = sb.toString().trim();

            String strHashedSign = DatatypeConverter.printHexBinary(bSignature).toUpperCase();
            return strHashedSign;

        } catch (Exception err) {
        }
        
        return null;
    }

    //! reference: https://github.com/square/okhttp/blob/master/samples/guide/src/main/java/okhttp3/recipes/PostMultipart.java
    public static Boolean sendToFR(String strFilePath, String strURL, String strServerDomain, String strServerEndPoint,
            Integer strVisitorID) {
        String strFileName = Integer.toString(strVisitorID) + ".jpg";
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("avatarFile",
                strFileName, RequestBody.create(new File(strFilePath), MediaType.get("image/jpg"))).build();

        Request request = new Request.Builder().url(strURL).post(requestBody).build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code" + response);
            }
            System.out.println(response.body().string());
            response.body().close();
            return true;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}

