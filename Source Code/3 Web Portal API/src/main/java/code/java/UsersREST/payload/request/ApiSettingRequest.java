package code.java.UsersREST.payload.request;

public class ApiSettingRequest {

    private Long id;
    private String setting_name;
    private String type;
    private String value;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getSetting_name() {
        return setting_name;
    }
    public void setSetting_name(String setting_name) {
        this.setting_name = setting_name;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    





    
    
}
