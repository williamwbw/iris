package code.java.UsersREST.WorkFlowModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "wfsetup_details")
public class WFSetupDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private String employeeids;

    // @ManyToOne(fetch = FetchType.EAGER)
    // @JoinColumn(name = "position", referencedColumnName = "id")
    private Integer positionID;

    // @ManyToOne(fetch = FetchType.EAGER)
    // @JoinColumn(name = "role", referencedColumnName = "id")
    private Integer roleID;

    private Integer wf_id;

    public WFSetupDetail() {
    }

    public WFSetupDetail(Integer id, String title, String employeeids, Integer positionID, Integer roleID,Integer wf_id) {
        this.id = id;
        this.title = title;
        this.employeeids = employeeids;
        this.wf_id = wf_id;
        this.positionID=positionID;
        this.roleID=roleID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmployeeID() {
        return employeeids;
    }

    public void setEmployeeID(String employeeids) {
        this.employeeids = employeeids;
    }

    public Integer getPosition() {
        return positionID;
    }

    public void setPosition(Integer positionID) {
        this.positionID = positionID;
    }

    public Integer getRole() {
        return roleID;
    }

    public void setRole(Integer roleID) {
        this.roleID = roleID;
    }

    public Integer getWID() {
        return wf_id;
    }

    public void setWFID(Integer wf_id) {
        this.wf_id = wf_id;
    }

}
