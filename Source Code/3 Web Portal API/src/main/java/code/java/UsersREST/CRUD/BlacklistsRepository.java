package code.java.UsersREST.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
 
public interface BlacklistsRepository extends JpaRepository<Blacklists, Integer> {

    // return Blacklist when status active
    @Query(value = "select * from blacklists where status = 'active' ", nativeQuery = true)
    public List<Blacklists> getBlackList();

    @Query(value="select JSON_OBJECT('id',ROW_NUMBER() OVER (),'name',name,'icpassport',icpassport,'reason',reason,'from_date',from_date,'to_date',to_date,'date_time',date_time,'blacklist_by',blacklist_by,'timestamp',CAST(UNIX_TIMESTAMP(date_time)*1000 as UNSIGNED)) from blacklists",nativeQuery = true)
    public List<Object> getAllBlacklistReport();
   
    @Query(value = "SELECT * FROM blacklists WHERE icpassport = ?1", nativeQuery = true)
    public Blacklists findByIcpassport(String icpassport); 

    
 
}
