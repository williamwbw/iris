package code.java.UsersREST.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import code.java.UsersREST.models.User;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository repo;

    public List<User> listAll() {
        return repo.findAll();
    }

    public void save(User users) {
        repo.save(users);
    }

    public User update(User users) {
        return repo.save(users);
    }

    public User get(Long long1) {
        return repo.findById(long1).get();
    }

}