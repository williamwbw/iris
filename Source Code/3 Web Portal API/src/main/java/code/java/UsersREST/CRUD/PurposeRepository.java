package code.java.UsersREST.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
 
public interface PurposeRepository extends JpaRepository<Purpose, Integer> {

     // return Purpose when status active
     @Query(value = "select * from purpose where status = 'active' ", nativeQuery = true)
     public List<Purpose> getPurposeList();

     @Query(value = "select id from purpose where status = 'active' and purposename = ?1", nativeQuery = true)
     public String getMapPurpose(String PurposeName);
 
}