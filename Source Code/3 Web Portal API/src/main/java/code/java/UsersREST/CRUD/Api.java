package code.java.UsersREST.CRUD;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Api {
    private Integer id;
    private String apiname;
    private String ip;
    private String status;
    private Integer createuserid;
    private String createdatetime;
    private Integer modifyuserid;
    private String modifydatetime;

    public Api() {
    }

    public Api(Integer id, String apiname, String ip, String status, Integer createuserid, String createdatetime, Integer modifyuserid, String modifydatetime) {
        this.id = id;
        this.apiname = apiname;
        this.ip = ip;
        this.status=status;
        this.createuserid = createuserid;
        this.createdatetime = createdatetime;
        this.modifyuserid = modifyuserid;
        this.modifydatetime = modifydatetime;     
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApiname() {
        return apiname;
    }

    public void setApiname(String apiname) {
        this.apiname = apiname;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getmodifydatetime() {
        return modifydatetime;
    }

    public String getcreatedatetime() {
        return createdatetime;
    }

    public Integer getmodifyuserid() {
        return modifyuserid;
    }

    public Integer getcreateuserid() {
        return createuserid;
    }

    public void setmodifyuserid(Integer modifyuserid) {
        this.modifyuserid = modifyuserid;
    }

    public void setcreateuserid(Integer createuserid) {
        this.createuserid = createuserid;
    }

    public void setmodifydatetime(String modifydatetime) {
        this.modifydatetime = modifydatetime;
    }

    public void setcreatedatetime(String createdatetime) {
        this.createdatetime = createdatetime;
    }
}
