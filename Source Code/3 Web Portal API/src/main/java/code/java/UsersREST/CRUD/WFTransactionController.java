package code.java.UsersREST.CRUD;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import code.java.UsersREST.CRUD.Visitors;
import code.java.UsersREST.CRUD.VisitorsService;
import code.java.UsersREST.WorkFlowModel.WFLog;
import code.java.UsersREST.WorkFlowModel.WFSetup;
import code.java.UsersREST.WorkFlowModel.WFTransaction;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/wftransaction")
public class WFTransactionController {

    @Autowired
    private WFTransactionService service;

    @Autowired
    private WFLogService logservice;

    @Autowired
    private WFTransactionRepository transrepo;

    @Autowired
    private WFSetupRepository setuprepo;

    @Autowired
    private VisitorsService visitservice;

    @Autowired
    private WFSetupService setupservice;

    @GetMapping("/list")
    public List<WFTransaction> list() {
        return service.listAll();
    }

    // get workflow bar but the approval value 
    @GetMapping("/get/visitorid={visitorid}")
    public List<WFTransaction> getTransaction(@PathVariable("visitorid") Integer visitorid) {
        List<WFTransaction> transaction = transrepo.getVisitorTransaction(visitorid);
        
        return transaction;
    }

    // get workflow bar with position name
    @GetMapping("/list/visitorid={visitorid}")
    public List<Object> getTransactions(@PathVariable("visitorid") Integer visitorid) {
        List<Object> transaction = transrepo.getTransactions(visitorid);
        
        return transaction;
    }

    @GetMapping("/add/visitorid={visitorid}")
    public void add(@PathVariable("visitorid") Integer visitorid) {

        Visitors visitors = visitservice.get(visitorid);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());

            // Insert data into wftransaction table
            WFTransaction workflow_transaction = new WFTransaction();
            workflow_transaction.setcreatedatetime(formatter.format(date));
            workflow_transaction.setmodifydatetime(formatter.format(date));
            workflow_transaction.setSubmitDatetime(formatter.format(date));
            workflow_transaction.setStatus("Pending");
            workflow_transaction.setVisitorID(visitors.getid());
            workflow_transaction.setWorkflowID(0);
            workflow_transaction.setStage(1);
            workflow_transaction.setcreateuserid(visitors.getcreateuserid());
            workflow_transaction.setmodifyuserid(visitors.getcreateuserid());
            service.save(workflow_transaction);

            // Insert data into wflog table
            WFLog workflow_log = new WFLog();
            workflow_log.setcreatedatetime(formatter.format(date));
            workflow_log.setmodifydatetime(formatter.format(date));
            workflow_log.setAction("Create Workflow Transaction");
            workflow_log.setRemarks(workflow_log.getRemarks());
            workflow_log.setRefTable("Workflow Transaction");
            workflow_log.setRefId(workflow_transaction.getId());
            workflow_log.setcreateuserid(visitors.getcreateuserid());
            workflow_log.setmodifyuserid(visitors.getcreateuserid());
            logservice.save(workflow_log);

    }

    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody WFTransaction workflow_transaction, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            WFTransaction existWorkflowTransaction = service.get(id);
            existWorkflowTransaction.setStatus(workflow_transaction.getStatus());
            existWorkflowTransaction.setRemarks(workflow_transaction.getRemarks());
            existWorkflowTransaction.setmodifyuserid(workflow_transaction.getmodifyuserid());
            existWorkflowTransaction.setmodifydatetime(formatter.format(date));
            existWorkflowTransaction.setStage(0);
            service.save(existWorkflowTransaction);

            Integer nextStage = transrepo.getNextStage(existWorkflowTransaction.getVisitorID());

            String status = workflow_transaction.getStatus();

            switch (status) {
                case "Approve":
                    if (nextStage != null) {
                        WFTransaction nextWFTransaction = service.get(nextStage);
                        nextWFTransaction.setStage(1);
                        nextWFTransaction.setmodifyuserid(workflow_transaction.getmodifyuserid());
                        nextWFTransaction.setmodifydatetime(formatter.format(date));
                        service.save(nextWFTransaction);

                    } else {

                        Visitors visitors = visitservice.get(existWorkflowTransaction.getVisitorID());
                        visitors.setStatus(workflow_transaction.getStatus());
                        visitors.setmodifyuserid(workflow_transaction.getmodifyuserid());
                        visitors.setmodifydatetime(formatter.format(date));
                        visitservice.save(visitors);
                    }
                    break;
                case "Reject":
                    // code block
                    transrepo.updateAllReject(existWorkflowTransaction.getVisitorID());

                    Visitors visitors = visitservice.get(existWorkflowTransaction.getVisitorID());
                    visitors.setStatus(workflow_transaction.getStatus());
                    visitors.setmodifyuserid(workflow_transaction.getmodifyuserid());
                    visitors.setmodifydatetime(formatter.format(date));
                    visitservice.save(visitors);
                    break;
                default:
                    // code block
            }

            // For Workflow Log
            WFLog workflow_log = new WFLog();
            workflow_log.setcreatedatetime(formatter.format(date));
            workflow_log.setmodifydatetime(formatter.format(date));
            workflow_log.setAction(workflow_transaction.getStatus());
            workflow_log.setRemarks(workflow_transaction.getRemarks());
            workflow_log.setcreateuserid(workflow_transaction.getmodifyuserid());
            workflow_log.setmodifyuserid(workflow_transaction.getmodifyuserid());
            workflow_log.setRefTable("Workflow Transaction");
            workflow_log.setRefId(existWorkflowTransaction.getId());

            logservice.save(workflow_log);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }

}
