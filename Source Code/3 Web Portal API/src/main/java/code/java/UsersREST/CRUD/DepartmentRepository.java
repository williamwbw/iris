package code.java.UsersREST.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
 
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
 
     // return Department when status active
     @Query(value = "select * from department where status = 'active' ", nativeQuery = true)
     public List<Department> getDepartmentList();
}