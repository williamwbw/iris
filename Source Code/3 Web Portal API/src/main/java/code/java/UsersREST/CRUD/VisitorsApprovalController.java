package code.java.UsersREST.CRUD;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.http.HttpClient;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.NoSuchElementException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.print.attribute.standard.Media;

import java.io.UnsupportedEncodingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import code.java.UsersREST.CRUD.WFTransactionService;
import code.java.UsersREST.WorkFlowModel.WFTransaction;
import reactor.core.publisher.Mono;

import org.springframework.http.client.MultipartBodyBuilder;

@RestController
@RequestMapping("/api/visitorapproval")
public class VisitorsApprovalController {
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Autowired
    private VisitorsService service;

    @Autowired
    private WFTransactionService wfservice;


    public void sendEmailApprove(String recipientEmail, String recipientName)
    throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();              
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("support@iris.com", "Iris Support");
        helper.setTo(recipientEmail);
        
        String subject = "Visitor Approval Status";
        
        String content = "<p>Hello "+recipientName+",</p>"
                + "<p>Congratulation!</p>"
                + "<p>We're here to inform you that your request has been successfully approved!</p>"
                + "<br>"
                + "<p>This email is auto generated </p>";
        
        helper.setSubject(subject);
        
        helper.setText(content, true);
        
        mailSender.send(message);
        }

    public void sendEmailReject(String recipientEmail, String recipientName) throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();              
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("support@iris.com", "Iris Support");
        helper.setTo(recipientEmail);
        
        String subject = "Visitor Approval Status";
        
        String content = "<p>Hello "+recipientName+",</p>"
                + "<p>We're sorry to inform you that your request has been rejected!</p>"
                + "<br>"
                + "<p>This email is auto generated </p>";
        
        helper.setSubject(subject);
        
        helper.setText(content, true);
        
        mailSender.send(message);
        }
    
        
    @CrossOrigin(origins = "http://localhost:8080")
    @PutMapping("/approve/id={id}")
    public ResponseEntity<?> approve(@RequestBody Visitors visitors, @PathVariable Integer id) throws UnsupportedEncodingException, MessagingException {
        try {
            ApiSetting setting = new ApiSetting();
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            String email = visitors.getEmail();
            
           
            Visitors existVisitors = service.get(id);
            new Thread(() -> {
                try{
                    sendEmailApprove(email,existVisitors.getName());
                }catch(Exception e){

                }
                
            }).start();
            existVisitors.setRemarks(visitors.getRemarks());
            existVisitors.setStatus(visitors.getStatus());
            existVisitors.setmodifyuserid(visitors.getmodifyuserid());
            existVisitors.setmodifydatetime(formatter.format(date));
            service.save(existVisitors);
            
            // update table workflow transaction
            WFTransaction existWorkflowTransaction = wfservice.getVisitorTransaction2(id);
            existWorkflowTransaction.setStatus(visitors.getStatus());
            existWorkflowTransaction.setRemarks("Approved");
            existWorkflowTransaction.setmodifyuserid(visitors.getmodifyuserid());
            existWorkflowTransaction.setmodifydatetime(formatter.format(date));
            wfservice.save(existWorkflowTransaction);

            //! Preparing signature for FR server            
            String strAppKey = setting.GetSetting("SecuritySettingAppKey", "App_Key");
            String strPrompt = setting.GetSetting("prompt", "Visitor");
            String strAreaCode = setting.GetSetting("areaCode", "Visitor");
            String strGroup = setting.GetSetting("groups", "Visitor");
            long timestamp = Instant.now().toEpochMilli();
            String strTimestamp = Long.toString(timestamp);
            String strSign = Utility.getMd5(strTimestamp);
            String strData = "name="+existVisitors.getName()
                    + "&mobile="+existVisitors.getContactNumber()
                    + "&remark="+existVisitors.getRemarks()
                    + "&groups="+ strGroup
                    + "&receptionUserId=" + existVisitors.getReceptionUserID()
                    + "&dateTimeFrom="+ existVisitors.getFromDate()
                    + "&dateTimeTo="+ existVisitors.getToDate()
                    + "&mail="+ existVisitors.getEmail()
                    + "&position=" + "null"
                    + "&guestCompany=" + existVisitors.getGuestCompany()
                    + "&guestsPurpose=" + existVisitors.getVisitPurpose()
                    + "&icNumber="+ existVisitors.getid()
                    + "&prompt="+ strPrompt
                    + "&areaCode="+ strAreaCode
                    + "&idNumber="+ existVisitors.getid()
                    + "&app_key="+ strAppKey
                    + "&timestamp="+ strTimestamp
                    + "&sign=" + strSign;
                    
            //! Start sending data to FRServer
            String serverIP = setting.GetSetting("FREmployeeDomain", "domain");
            String serverEndPoint = "/api/v1/guest?";
            String strURL = serverIP + serverEndPoint + strData;

            // CloseableHttpClient client - HttpClients.createDefault();
            // OkHttp3ClientHttpRequestFactory 
            String pathRest = new File("").getAbsolutePath();
            File pathRest2 = new File(pathRest);
            File pathVue = new File(pathRest2, "../2 Web Portal/public/upload/" + visitors.getid() + "." + ".jpg");
            String strFileLocation = pathVue.getCanonicalPath();
            // String strFileLocation = "";
            // RequestBody requestBody = new MultipartBody.Builder()
            //     .setType(MultipartBody.FORM)
            //     .addFormDataPart("avatarFile", strFileLocation,
            //         RequestBody.create(strFileLocation,MediaType.parse("image/*")))
            //     .build();

            System.out.println(strURL);
            //? okhttp3 built (not tested)
            
            Boolean success = Utility.sendToFR(strFileLocation, strURL, serverIP, serverEndPoint,visitors.getid());

            
            //? webclient built (not tested)
            // WebClient webClient = webClientBuilder.build();
            // MultipartBodyBuilder builder = new MultipartBodyBuilder();
            // Resource avatar = new ClassPathResource(strFileLocation);
            // builder.part("avatarFile", avatar);
            // Mono<HttpStatus> result = webClient
            //     .post()
            //     .uri("localhost:44359/FRLogin")
            //     .contentType(MediaType.MULTIPART_FORM_DATA)
            //     .body(BodyInserters.fromMultipartData(builder.build()))
            //     .exchangeToMono(response->{
            //         if(response.statusCode().equals(HttpStatus.OK)){
            //             return response.bodyToMono(HttpStatus.class).thenReturn(response.statusCode());
            //         }else{
            //             return response.bodyToMono(HttpStatus.class).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
            //         }
            //     });
  

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

        @CrossOrigin(origins = "http://localhost:8080")
    @PutMapping("/reject/id={id}")
    public ResponseEntity<?> reject(@RequestBody Visitors visitors, @PathVariable Integer id) throws UnsupportedEncodingException, MessagingException {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            String email = visitors.getEmail();
            Visitors existVisitors = service.get(id);
            new Thread(() -> {
                try{
                    sendEmailReject(email,existVisitors.getName());
                }catch(Exception e){

                }                
            }).start();
            existVisitors.setRemarks(visitors.getRemarks());
            existVisitors.setStatus(visitors.getStatus());
            existVisitors.setmodifyuserid(visitors.getmodifyuserid());
            existVisitors.setmodifydatetime(formatter.format(date));
            service.save(existVisitors);

            // update table workflow transaction
            WFTransaction existWorkflowTransaction = wfservice.getVisitorTransaction2(id);
            existWorkflowTransaction.setStatus("Reject");
            existWorkflowTransaction.setRemarks("Rejected");
            existWorkflowTransaction.setmodifyuserid(visitors.getmodifyuserid());
            existWorkflowTransaction.setmodifydatetime(formatter.format(date));
            wfservice.save(existWorkflowTransaction);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
