package code.java.UsersREST.CRUD;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

interface getTheValue {

    String value();
}

public interface ApiSettingRepository extends JpaRepository<ApiSetting, Long>{

    @Query(value = "select * from api_setting ", nativeQuery = true)
    public List<ApiSetting> getApiSettingList();

    @Query(value = "delete * from api_setting a where a.id=:id", nativeQuery = true)
    public void delete(Long id);

    @Query(value="select * from api_setting where type = 'App_Secret'", nativeQuery = true)
    public List<ApiSetting> getAppSecret();

    @Query(value="select * from api_setting where setting_name = setting_name AND type = type", nativeQuery = true)
    public ApiSetting GetSetting();


    List<ApiSetting> findByTypeContaining(String type);

    List<getTheValue> findAllBy();


}



