package code.java.UsersREST.CRUD;

import java.util.NoSuchElementException;
import java.io.UnsupportedEncodingException;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import code.java.UsersREST.CRUD.model.Users;
import code.java.UsersREST.payload.request.ForgotPasswordRequest;
import code.java.UsersREST.payload.request.ResetPasswordRequest;
import code.java.UsersREST.payload.response.MessageResponse;
import code.java.UsersREST.repository.UserRepository;
import net.bytebuddy.utility.RandomString;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/fp")
public class ForgotPasswordController {
    @Autowired
    private JavaMailSender mailSender;
     
    @Autowired
    private UsersService usersService;

    @Autowired
    private UserRepository userRepository;
     
    @Autowired
    private UsersRepository usersRepository;

   
    public void sendEmail(String recipientEmail, String link)
    throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();              
        MimeMessageHelper helper = new MimeMessageHelper(message);
        // to replace the port from 1235 to 8080    
        // UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(link);
        // String url2 = builder.port("8080").toUriString();
        
        
        helper.setFrom("support@iris.com", "Iris Support");
        helper.setTo(recipientEmail);
        
        String subject = "Here's the link to reset your password";
        
        String content = "<p>Hello,</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to reset your password:</p>"
                + "<p><a href=\"" + link + "\">Reset my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";
        
        helper.setSubject(subject);
        
        helper.setText(content, true);
        
        mailSender.send(message);
        }



        @PostMapping("/forgot_password")
        public ResponseEntity<?> ForgotPassword(HttpServletRequest request, @Valid @RequestBody ForgotPasswordRequest ForgotPasswordRequest) throws Exception {
         
            // Check if email exist in database

            Users users = usersRepository.findByEmail(ForgotPasswordRequest.getEmail());
            if (users != null) {
           
                String token = RandomString.make(30);
                String email = ForgotPasswordRequest.getEmail();
                usersService.updateResetPasswordToken(token, email);
    
                //String resetPasswordLink = Utility.getSiteURL(request) + "/#/ResetPassword?token=" + token;
                
                ApiSetting objSetting = new ApiSetting();
                String CHost = objSetting.GetSetting("ClientHost", "IP");
                String CPort = objSetting.GetSetting("ClientPort", "Port");

                String resetPasswordLink = "http://"+ CHost + ":" + CPort + "/#/ResetPassword?token=" + token;
                
                sendEmail(email, resetPasswordLink);	
                return ResponseEntity.ok(new MessageResponse("Email sent successfully!" + token));
                
                }

                else {
                    //return ResponseEntity.ok(new MessageResponse("None!"));
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
        }
    


    @PostMapping("/reset_password")
    public ResponseEntity<?> processResetPassword(@Valid @RequestBody ResetPasswordRequest ResetPasswordRequest) {
        //if (userRepository.findByResetPasswordToken(ResetPasswordRequest.getToken())) {
    

        String token = ResetPasswordRequest.getToken();
        String password = ResetPasswordRequest.getPassword();
    
        Users users = usersService.getByResetPasswordToken(token);
        
        if (users != null) {
            usersService.updatePassword(users, password);
            //return ResponseEntity.ok(new MessageResponse("You have successfully changed your password.!"));
            return new ResponseEntity<>(HttpStatus.OK);
        }
             
        //return ResponseEntity.ok(new MessageResponse("Invalid Token!"));
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    
   
    }
}
