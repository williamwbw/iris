
package code.java.UsersREST.CRUD;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private ApiService service;

    @Autowired
    private ApiRepository repo;

    @GetMapping("/list")
    public List<Api> list() {
        return repo.getApiList();
    }

    @GetMapping("/list/id={id}")
    public ResponseEntity<Api> get(@PathVariable Integer id) {
        try {
            Api api = service.get(id);
            return new ResponseEntity<Api>(api, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Api>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public void add(@RequestBody Api api) {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        api.setcreatedatetime(formatter.format(date));
        api.setmodifydatetime(formatter.format(date));
        service.save(api);
    }

    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody Api api, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            Api existApi = service.get(id);
            existApi.setApiname(api.getApiname());
            existApi.setIp(api.getIp());
            existApi.setStatus(api.getStatus());
            existApi.setmodifyuserid(api.getmodifyuserid());
            existApi.setmodifydatetime(formatter.format(date));
            service.save(existApi);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
} 
