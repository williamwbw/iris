package code.java.UsersREST.controllers;

import java.security.Provider.Service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.mysql.cj.x.protobuf.MysqlxCrud.Update;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import code.java.UsersREST.models.ERole;
import code.java.UsersREST.models.Role;
import code.java.UsersREST.models.User;
import code.java.UsersREST.payload.request.ChangePasswordRequest;
import code.java.UsersREST.payload.request.EditRequest;
import code.java.UsersREST.payload.request.LoginRequest;
import code.java.UsersREST.payload.request.LogoutRequest;
import code.java.UsersREST.payload.request.SignupRequest;
import code.java.UsersREST.payload.response.JwtResponse;
import code.java.UsersREST.payload.response.MessageResponse;
import code.java.UsersREST.repository.RoleRepository;
import code.java.UsersREST.repository.UserRepository;
import code.java.UsersREST.repository.UserService;
import code.java.UsersREST.security.jwt.JwtUtils;
import code.java.UsersREST.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	UserService userService;

	@PostMapping("/logout")
	public ResponseEntity<?> logout(@Valid @RequestBody LogoutRequest logoutRequest) {

		try {

			if (logoutRequest.getAccessToken() != null) {

				User user = userService.get(logoutRequest.getId());
				user.setAccessToken(null);
				userService.update(user);
			}
			return ResponseEntity.ok(new MessageResponse("Logout successfully!"));

		} catch (Exception e) {
			System.out.println(e);
			return ResponseEntity.ok(new MessageResponse("Logout Fail!"));
		}
	}

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		String token = jwt;

		User user = userService.get(userDetails.getId());
		user.setAccessToken(token);
		userService.update(user);
		
		return ResponseEntity.ok(
				new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());
		// Create new user's account
		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()), signUpRequest.getAccessToken(), signUpRequest.getStatus(),
				signUpRequest.getcreateuserid(), signUpRequest.getDateTime(), signUpRequest.getmodifyuserid(),
				signUpRequest.getmodifydatetime());

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@PutMapping("/update/id={id}")
	public ResponseEntity<?> editUser(@Valid @RequestBody EditRequest EditRequest, @PathVariable Long id) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());
		User user = userService.get(id);
		user.setUsername(EditRequest.getUsername());
		user.setEmail(EditRequest.getEmail());
		user.setPassword(encoder.encode(EditRequest.getPassword()));
		user.setStatus(EditRequest.getStatus());
		user.setmodifyuserid(EditRequest.getmodifyuserid());
		user.setmodifydatetime(formatter.format(date));
		// user.setEmployeeid(EditRequest.getEmployeeid());

		Set<String> strRoles = EditRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userService.update(user);
		return ResponseEntity.ok(new MessageResponse("User Edit successfully!"));
	}

	@PutMapping("/ChangePassword/id={id}")
	public ResponseEntity<?> ChangePassword(@Valid @RequestBody ChangePasswordRequest ChangePasswordRequest,
			@PathVariable Long id) {

		User user = userService.get(id);
		user.setPassword(encoder.encode(ChangePasswordRequest.getPassword()));
		userService.update(user);
		return ResponseEntity.ok(new MessageResponse("Password Changed successfully!"));
	}

}