package code.java.UsersREST.CRUD;

import java.util.List;
 
import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
@Service
@Transactional
public class ApiService {
 
    @Autowired
    private ApiRepository repo;
     
    public List<Api> listAll() {
        return repo.findAll();
    }
     
    public void save(Api api) {
        repo.save(api);
    }
     
    public Api get(Integer id) {
        return repo.findById(id).get();
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }
}
