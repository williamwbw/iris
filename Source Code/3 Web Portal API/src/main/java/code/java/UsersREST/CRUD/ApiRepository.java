package code.java.UsersREST.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
 
public interface ApiRepository extends JpaRepository<Api, Integer> {
 
    @Query(value = "select * from api where status = 'enable'", nativeQuery = true)
    public List<Api> getApiList();
    
}