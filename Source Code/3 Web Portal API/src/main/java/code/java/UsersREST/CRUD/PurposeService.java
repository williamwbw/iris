package code.java.UsersREST.CRUD;

import java.util.List;
 
import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
@Service
@Transactional
public class PurposeService {
 
    @Autowired
    private PurposeRepository repo;
     
    public List<Purpose> listAll() {
        return repo.findAll();
    }
     
    public void save(Purpose purpose) {
        repo.save(purpose);
    }
     
    public Purpose get(Integer id) {
        return repo.findById(id).get();
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }
}
