package code.java.UsersREST.CRUD;

import java.util.List;
 
import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
@Service
@Transactional
public class DepartmentService {
 
    @Autowired
    private DepartmentRepository repo;
     
    public List<Department> listAll() {
        return repo.findAll();
    }
     
    public void save(Department department) {
        repo.save(department);
    }
     
    public Department get(Integer id) {
        return repo.findById(id).get();
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }
}
