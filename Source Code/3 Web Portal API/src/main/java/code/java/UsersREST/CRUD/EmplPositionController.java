package code.java.UsersREST.CRUD;

import java.util.List;
import java.util.NoSuchElementException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/emplposition")
public class EmplPositionController {

    @Autowired
    private EmplPositionService service;

    @Autowired
    private EmplPositionRepository repo;

   
    @GetMapping("/list")
    public List<EmplPosition> list() {
        return repo.getPositionList();
    }

    
    @GetMapping("/list/{id}")
    public ResponseEntity<EmplPosition> get(@PathVariable Integer id) {
        try {
            EmplPosition empl_position = service.get(id);
            return new ResponseEntity<EmplPosition>(empl_position, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<EmplPosition>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public void add(@RequestBody EmplPosition empl_position) {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        empl_position.setcreatedatetime(formatter.format(date));
        empl_position.setmodifydatetime(formatter.format(date));
        service.save(empl_position);
    }

    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody EmplPosition empl_position, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            EmplPosition existEmplPosition = service.get(id);
            existEmplPosition.setpositionname(empl_position.getpositionname());
            existEmplPosition.setStatus(empl_position.getstatus());
            existEmplPosition.setmodifyuserid(empl_position.getmodifyuserid());
            existEmplPosition.setmodifydatetime(formatter.format(date));
            service.save(existEmplPosition);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/id={id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}