package code.java.UsersREST.CRUD;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

 
public interface EmplPositionRepository extends JpaRepository<EmplPosition, Integer> {

    // return Position when status active
    @Query(value = "select * from empl_position where status = 'active' ", nativeQuery = true)
    public List<EmplPosition> getPositionList();
}