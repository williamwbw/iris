package code.java.UsersREST.CRUD;

import java.util.List;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import code.java.UsersREST.WorkFlowModel.WFLog;

@Service
@Transactional
public class WFLogService {

    @Autowired
    private WFLogRepository repo;

    public List<WFLog> listAll() {
        return repo.findAll();
    }

    public void save(WFLog workflow_log) {
        repo.save(workflow_log);
    }

    public WFLog get(Integer id) {
        return repo.findById(id).get();
    }

}