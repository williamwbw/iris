package code.java.UsersREST.CRUD;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Blacklists {
    private Integer id;
    private String name;
    private String icpassport;
    private String reason;
    private String from_date;
    private String to_date;
    private String date_time;
    private String blacklist_by;
    private String approve_by;
    private String status;
    private Integer createuserid;
    private String createdatetime;
    private Integer modifyuserid;
    private String modifydatetime;

public Blacklists() {  }

    public Blacklists(Integer id, String name, String icpassport, String reason, String from_date,String to_date,String date_time,String blacklist_by,String approve_by, String status,
    Integer createuserid, String createdatetime, Integer modifyuserid, String modifydatetime) 
    {
        this.id=id;
        this.name=name;
        this.icpassport=icpassport;
        this.reason=reason;
        this.from_date=from_date;
        this.to_date=to_date;
        this.date_time=date_time;
        this.blacklist_by=blacklist_by;
        this.approve_by=approve_by;
        this.status = status;
        this.createuserid = createuserid;
        this.createdatetime = createdatetime;
        this.modifyuserid = modifyuserid;
        this.modifydatetime = modifydatetime;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getid() {
        return id;
    }

    public void setid(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getICPassport() {
        return icpassport;
    }

    public void setICPassport(String icpassport) {
        this.icpassport = icpassport;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFromDate() {
        return from_date;
    }

    public void setFromDate(String from_date) {
        this.from_date = from_date;
    }

    public String getToDate() {
        return to_date;
    }

    public void setToDate(String to_date) {
        this.to_date = to_date;
    }

    public String getDateTime() {
        return date_time;
    }

    public void setDateTime(String date_time) {
        this.date_time = date_time;
    }

    public String getBlacklistBy() {
        return blacklist_by;
    }

    public void setBlacklistBy(String blacklist_by) {
        this.blacklist_by = blacklist_by;
    }

    public String getApproveBy() {
        return approve_by;
    }

    public void setApproveBy(String approve_by) {
        this.approve_by = approve_by;
    }

    public String getstatus() {
        return status;
    }

    public String getmodifydatetime() {
        return modifydatetime;
    }

    public String getcreatedatetime() {
        return createdatetime;
    }

    public Integer getmodifyuserid() {
        return modifyuserid;
    }

    public Integer getcreateuserid() {
        return createuserid;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setmodifyuserid(Integer modifyuserid) {
        this.modifyuserid = modifyuserid;
    }

    public void setcreateuserid(Integer createuserid) {
        this.createuserid = createuserid;
    }

    public void setmodifydatetime(String modifydatetime) {
        this.modifydatetime = modifydatetime;
    }

    public void setcreatedatetime(String createdatetime) {
        this.createdatetime = createdatetime;
    }
}
