package code.java.UsersREST.payload.request;

import java.util.Set;

import javax.validation.constraints.*;
 
public class EditRequest {

    private Long id;

    @NotBlank
    private String username;

    private String email;
    
    private Set<String> role;
  
    private String status;
    private Integer createuserid;
    private String createdatetime;
    private Integer modifyuserid;
    private String modifydatetime;
    private String password;
    private String resetPasswordToken;
    
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Set<String> getRole() {
      return this.role;
    }
    
    public void setRole(Set<String> role) {
      this.role = role;
    }


    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

  public String getStatus() {
    return status;
}

public void setStatus(String status) {
    this.status = status;
}

public String getmodifydatetime() {
    return modifydatetime;
}

public String getcreatedatetime() {
    return createdatetime;
}

public Integer getmodifyuserid() {
    return modifyuserid;
}

public Integer getcreateuserid() {
    return createuserid;
}

public void setmodifyuserid(Integer modifyuserid) {
    this.modifyuserid = modifyuserid;
}

public void setcreateuserid(Integer createuserid) {
    this.createuserid = createuserid;
}

public void setmodifydatetime(String modifydatetime) {
    this.modifydatetime = modifydatetime;
}

public void setcreatedatetime(String createdatetime) {
    this.createdatetime = createdatetime;
}

public String getResetPasswordToken() {
    return resetPasswordToken;
}

public void setResetPasswordToken(String resetPasswordToken) {
    this.resetPasswordToken = resetPasswordToken;
}


    
}
