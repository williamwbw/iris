package code.java.UsersREST.CRUD;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ApiSettingService {

    @Autowired
    private ApiSettingRepository repo;

    //service
    public List<ApiSetting> listAll() {
        return repo.findAll();
    }

    public void save(ApiSetting apiSetting) {
        repo.save(apiSetting);
    }

    public ApiSetting get(Long id) {
        return repo.findById(id).get();
    }

    public void update(ApiSetting apiSetting) {

    }

    public void delete(Long id) {
        repo.deleteById(id);
    }


    public ApiSetting GetSettingg() {
        return repo.GetSetting();
    }

    public List<ApiSetting> getAppSecrett() {
        return repo.getAppSecret();
    }

    
    
}
