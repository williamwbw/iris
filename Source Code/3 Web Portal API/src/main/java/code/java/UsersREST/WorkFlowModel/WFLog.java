package code.java.UsersREST.WorkFlowModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "workflow_log")
public class WFLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String action; // 1. Create 2. Modify 3. Approve 4. Reject 5. Delete
    private String remarks;
    private Integer createuserid;
    private String createdatetime;
    private Integer modifyuserid;
    private String modifydatetime;

    // private Integer wfsetup_id;
    // private Integer wftransaction_id;
    private String ref_table;

    // @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ref_id;

    // //@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    // @ManyToOne(fetch = FetchType.EAGER)
    // @JoinColumn(name = "wf_wsid", referencedColumnName = "id")
    // private WFSetup workflow_setup;

    // //@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    // @ManyToOne(fetch = FetchType.EAGER)
    // @JoinColumn(name = "wf_wtid", referencedColumnName = "id")
    // private WFTransaction workflow_transaction;

    public WFLog() {
    }

    public WFLog(String action, String remarks, Integer createuserid, String createdatetime, Integer modifyuserid,
            String modifydatetime, String ref_table, Integer ref_id) {

        this.action = action;
        this.remarks = remarks;
        this.createuserid = createuserid;
        this.createdatetime = createdatetime;
        this.modifyuserid = modifyuserid;
        this.modifydatetime = modifydatetime;
        // this.wfsetup_id = wfsetup_id;
        // this.wftransaction_id = wftransaction_id;
        this.ref_table = ref_table;
        this.ref_id = ref_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getmodifydatetime() {
        return modifydatetime;
    }

    public String getcreatedatetime() {
        return createdatetime;
    }

    public Integer getmodifyuserid() {
        return modifyuserid;
    }

    public Integer getcreateuserid() {
        return createuserid;
    }

    public void setmodifyuserid(Integer modifyuserid) {
        this.modifyuserid = modifyuserid;
    }

    public void setcreateuserid(Integer createuserid) {
        this.createuserid = createuserid;
    }

    public void setmodifydatetime(String modifydatetime) {
        this.modifydatetime = modifydatetime;
    }

    public void setcreatedatetime(String createdatetime) {
        this.createdatetime = createdatetime;
    }

    // public Integer getWorkflowID() {
    // return wfsetup_id;
    // }

    // public void setWorkflowID(Integer wfsetup_id) {
    // this.wfsetup_id = wfsetup_id;
    // }

    // public Integer getWorkflowTransID(){
    // return wftransaction_id;
    // }

    // public void setWorkflowTransID(Integer wftransaction_id){
    // this.wftransaction_id=wftransaction_id;
    // }

    public String getRefTable() {
        return ref_table;
    }

    public void setRefTable(String ref_table) {
        this.ref_table = ref_table;
    }

    public Integer getRefId() {
        return ref_id;
    }

    public void setRefId(Integer ref_id) {
        this.ref_id = ref_id;
    }

}