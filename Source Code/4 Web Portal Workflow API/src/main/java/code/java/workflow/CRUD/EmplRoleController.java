package code.java.workflow.CRUD;

import java.util.List;
import java.util.NoSuchElementException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/emplrole")
public class EmplRoleController {

    @Autowired
    private EmplRoleService service;

   
    @GetMapping("/list")
    public List<EmplRole> list() {
        return service.listAll();
    }

    
    @GetMapping("/list/{id}")
    public ResponseEntity<EmplRole> get(@PathVariable Integer id) {
        try {
            EmplRole empl_role = service.get(id);
            return new ResponseEntity<EmplRole>(empl_role, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<EmplRole>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public void add(@RequestBody EmplRole empl_role) {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        empl_role.setcreatedatetime(formatter.format(date));
        empl_role.setmodifydatetime(formatter.format(date));
        service.save(empl_role);
    }

    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody EmplRole empl_role, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            EmplRole existEmplRole = service.get(id);
            existEmplRole.setName(empl_role.getRoleName());
            existEmplRole.setStatus(empl_role.getstatus());
            existEmplRole.setmodifyuserid(empl_role.getmodifyuserid());
            existEmplRole.setmodifydatetime(formatter.format(date));
            service.save(existEmplRole);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/id={id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}