package code.java.workflow.WorkFlowModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "workflow_transaction")
public class WFTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String submitdatetime;
    private String status; // Submitted , Approve , Reject
    private String remarks;
    private Integer createuserid;
    private String createdatetime;
    private Integer modifyuserid;
    private String modifydatetime;

    private Integer wfsetup_id;
    private Integer visitorID;
    private Integer stage;

    // @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    // @ManyToOne(fetch = FetchType.EAGER)
    // @JoinColumn(name = "wf_wsid", referencedColumnName = "id")
    // private WFSetup workflow_setup;

    // @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    // @ManyToOne(fetch = FetchType.EAGER)
    // @JoinColumn(name = "wf_vid", referencedColumnName = "id")
    // private Visitors visitors;

    public WFTransaction() {
    }

    public WFTransaction(Integer id, String submitdatetime, String status, Integer createuserid, String createdatetime,
            Integer modifyuserid, String modifydatetime, Integer wfsetup_id,Integer visitorID, String remarks, Integer stage) {

        this.id = id;
        this.submitdatetime = submitdatetime;
        this.status = "Submitted";
        this.createuserid = createuserid;
        this.createdatetime = createdatetime;
        this.modifyuserid = modifyuserid;
        this.modifydatetime = modifydatetime;
        this.wfsetup_id=wfsetup_id;
        this.visitorID=visitorID;
        this.remarks=remarks;
        this.stage=stage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubmitDateTime() {
        return submitdatetime;
    }

    public void setSubmitDatetime(String submitdatetime) {
        this.submitdatetime = submitdatetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getmodifydatetime() {
        return modifydatetime;
    }

    public String getcreatedatetime() {
        return createdatetime;
    }

    public Integer getmodifyuserid() {
        return modifyuserid;
    }

    public Integer getcreateuserid() {
        return createuserid;
    }

    public void setmodifyuserid(Integer modifyuserid) {
        this.modifyuserid = modifyuserid;
    }

    public void setcreateuserid(Integer createuserid) {
        this.createuserid = createuserid;
    }

    public void setmodifydatetime(String modifydatetime) {
        this.modifydatetime = modifydatetime;
    }

    public void setcreatedatetime(String createdatetime) {
        this.createdatetime = createdatetime;
    }

    public Integer getWorkflowID(){
        return wfsetup_id;
    }

    public void setWorkflowID(Integer wfsetup_id){
        this.wfsetup_id=wfsetup_id;
    }

    public Integer getVisitorID(){
        return visitorID;
    }

    public void setVisitorID(Integer visitorID){
        this.visitorID=visitorID;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }
}
