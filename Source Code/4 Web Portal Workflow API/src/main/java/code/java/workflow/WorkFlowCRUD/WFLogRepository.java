package code.java.workflow.WorkFlowCRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import code.java.workflow.WorkFlowModel.WFLog;

public interface WFLogRepository extends JpaRepository<WFLog, Integer> {

    // get log
    @Query(value="SELECT JSON_OBJECT('id',logid , 'action',action, 'remarks',remarks,'visitorid',visitorid,'createdatetime',createdatetime,'positionname',positionname,'visitorname',visitorname,'employeename',employeename,'modifyuserid',modifyuserid) FROM view_wf_workflow_log where visitorid = ?1",nativeQuery = true)
    public List<Object> getLog(Integer id);

    @Query(value="SELECT JSON_OBJECT('id',id , 'action',action, 'remarks',remarks,'visitorid',visitorid,'createdatetime',createdatetime,'positionname',positionname,'visitorname',visitorname,'employeename',employeename,'timestamp',timestamp) FROM view_report_workflowlog",nativeQuery = true)
    public List<Object> getAllLogReport();
    
}
