package code.java.workflow.CRUD;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import code.java.workflow.WorkFlowCRUD.WFLogService;
import code.java.workflow.WorkFlowCRUD.WFTransactionService;
import code.java.workflow.WorkFlowModel.WFLog;
import code.java.workflow.WorkFlowModel.WFTransaction;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/visitor")
public class VisitorsController {
    
    @Autowired
    private VisitorsService service;

    @Autowired
    private VisitorsRepository repo;

    @Autowired
    private WFTransactionService transService;

    @Autowired
    private WFLogService logservice;


    @GetMapping("/list")
    public List<Visitors> list() {
        return service.listAll();
    }

     // get specific visitor with department purpose name
     @GetMapping("/list/id={id}")
     public List<Object> getVisitor(@PathVariable("id") Integer id) {
         List<Object> visitor = repo.getSpecificVisitor(id);
         
         return visitor;
     }
     
     @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/list/{id}")
    public ResponseEntity<Visitors> get(@PathVariable Integer id) {
        try {
            Visitors visitors = service.get(id);
            return new ResponseEntity<Visitors>(visitors, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Visitors>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/add/visitorid={visitorid}")
    public void add(@PathVariable("visitorid") Integer visitorid) {

        Visitors visitors = service.get(visitorid);
         SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         Date date = new Date(System.currentTimeMillis());

        // Insert data into wftransaction table
        WFTransaction workflow_transaction = new WFTransaction();
            workflow_transaction.setcreatedatetime(formatter.format(date));
            workflow_transaction.setmodifydatetime(formatter.format(date));
            workflow_transaction.setSubmitDatetime(formatter.format(date));
            workflow_transaction.setStatus("Pending");
            workflow_transaction.setVisitorID(visitors.getid());
            workflow_transaction.setWorkflowID(0);
            workflow_transaction.setStage(1);
            workflow_transaction.setcreateuserid(visitors.getcreateuserid());
            workflow_transaction.setmodifyuserid(visitors.getcreateuserid());
            transService.save(workflow_transaction);

            // Insert data into wflog table
            WFLog workflow_log = new WFLog();
            workflow_log.setcreatedatetime(formatter.format(date));
            workflow_log.setmodifydatetime(formatter.format(date));
            workflow_log.setAction("Create Workflow Transaction");
            workflow_log.setRemarks(workflow_log.getRemarks());
            workflow_log.setRefTable("Workflow Transaction");
            workflow_log.setRefId(workflow_transaction.getId());
            workflow_log.setcreateuserid(visitors.getcreateuserid());
            workflow_log.setmodifyuserid(visitors.getcreateuserid());
            logservice.save(workflow_log);
    }


    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody Visitors visitors, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            Visitors existVisitors = service.get(id);
            existVisitors.setStatus(visitors.getStatus());
            existVisitors.setmodifyuserid(visitors.getmodifyuserid());
            existVisitors.setmodifydatetime(formatter.format(date));
            service.save(existVisitors);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}
