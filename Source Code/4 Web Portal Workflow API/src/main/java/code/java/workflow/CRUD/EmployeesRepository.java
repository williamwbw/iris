package code.java.workflow.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface EmployeesRepository extends JpaRepository<Employees, Integer> {

    @Query(value = "select JSON_OBJECT('id',employees.id, 'name',employees.name) from employees inner join empl_role on employees.role = empl_role.id  inner join empl_position on employees.position = empl_position.id where employees.role = ?1 AND employees.position = ?2", nativeQuery = true)
    public List<Object> getEmployeeIdAndNames(String role, String position);

    @Query(value = "select JSON_OBJECT('id',employees.id, 'name',employees.name) from employees inner join empl_role on employees.role = empl_role.id  inner join empl_position on employees.position = empl_position.id where employees.role = ?1", nativeQuery = true)
    public List<Object> getEmployeeIdAndNamesByRole(String value);

    @Query(value = "select JSON_OBJECT('id',employees.id, 'name',employees.name) from employees inner join empl_role on employees.role = empl_role.id  inner join empl_position on employees.position = empl_position.id where employees.position = ?1", nativeQuery = true)
    public List<Object> getEmployeeIdAndNamesByPosition(String value);
    
    @Query(value = "select * from employees s where s.role = ?1 and s.position= ?2", nativeQuery = true)
    public List<Employees> getEmployeeIdAndNamess(String role, String position);





}