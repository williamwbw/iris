package code.java.workflow.WorkFlowCRUD;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import code.java.workflow.WorkFlowModel.WFLog;

import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/wflog")
public class WFLogController {

    @Autowired
    private WFLogService service;

    @Autowired
    private WFLogRepository repo;

    @GetMapping("/list")
    public List<WFLog> list() {
        return service.listAll();
    }

    @GetMapping("/report")
     public List<Object> report() {
         return repo.getAllLogReport();
     }

    @GetMapping("/list/id={id}")
    public ResponseEntity<WFLog> get(@PathVariable Integer id) {
        try {
            WFLog workflow_log = service.get(id);
            return new ResponseEntity<WFLog>(workflow_log, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<WFLog>(HttpStatus.NOT_FOUND);
        }
    }

    // get workflow log approval action with position name and visitor name
    @GetMapping("/list/visitorid={visitorid}")
    public List<Object> getLogDetails(@PathVariable("visitorid") Integer visitorid) {
        List<Object> log = repo.getLog(visitorid);
        
        
        return log;
    }

    @PostMapping("/add")
    public void add(@RequestBody WFLog workflow_log) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        workflow_log.setcreatedatetime(formatter.format(date));
        workflow_log.setmodifydatetime(formatter.format(date));
        workflow_log.setAction(workflow_log.getAction());
        workflow_log.setRemarks(workflow_log.getRemarks());
        workflow_log.setRefId(workflow_log.getRefId());
        workflow_log.setRefTable(workflow_log.getRefTable());

        service.save(workflow_log);
    }

}
