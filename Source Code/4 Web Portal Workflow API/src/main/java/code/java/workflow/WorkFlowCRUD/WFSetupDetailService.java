package code.java.workflow.WorkFlowCRUD;

import java.util.List;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import code.java.workflow.WorkFlowModel.WFSetupDetail;

@Service
@Transactional
public class WFSetupDetailService {

    @Autowired
    private WFSetupDetailRepository repo;

    public List<WFSetupDetail> listAll() {
        return repo.findAll();
    }

    public void save(WFSetupDetail wfsetup_details) {
        repo.save(wfsetup_details);
    }

    public WFSetupDetail get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }

}
