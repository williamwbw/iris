package code.java.workflow.CRUD;

import java.util.List;

import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class EmplRoleService {
    @Autowired
    private EmplRoleRepository repo;
     
    public List<EmplRole> listAll() {
        return repo.findAll();
    }
     
    public void save(EmplRole empl_role) {
        repo.save(empl_role);
    }
     
    public EmplRole get(Integer id) {
        return repo.findById(id).get();
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }
  
   
}
