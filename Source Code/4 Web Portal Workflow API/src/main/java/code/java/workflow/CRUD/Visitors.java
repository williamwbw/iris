package code.java.workflow.CRUD;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Visitors {
    private Integer id;
    private String time_stamp;
    private String name;
    private String gender;
    private String icnumber;
    private String contact_number;
    private String email;
    private String from_date;
    private String to_date;
    private Integer group_default;
    private String guest_company;
    private Integer reception_userid;
    private String remarks;
    private String visit_purpose;
    private String visit_department;
    private String status;
    private Integer createuserid;
    private String createdatetime;
    private Integer modifyuserid;
    private String modifydatetime;

    public Visitors() {  }

    public Visitors(Integer id, String time_stamp, String name, String gender,String icnumber,String contact_number,String email,String from_date,String to_date, Integer group_default, String guest_company, Integer reception_userid, String remarks, String visit_purpose, String visit_department, String status,
    Integer createuserid, String createdatetime, Integer modifyuserid, String modifydatetime) 
    {
        this.id=id;
        this.time_stamp=time_stamp;
        this.name=name;
        this.gender=gender;
        this.icnumber=icnumber;
        this.contact_number=contact_number;
        this.email=email;
        this.from_date=from_date;
        this.to_date=to_date;
        this.group_default=group_default;
        this.guest_company=guest_company;
        this.reception_userid=reception_userid;
        this.remarks=remarks;
        this.visit_purpose=visit_purpose;
        this.visit_department=visit_department;
        this.status="Pending";
        this.createuserid = createuserid;
        this.createdatetime = createdatetime;
        this.modifyuserid = modifyuserid;
        this.modifydatetime = modifydatetime;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getid() {
        return id;
    }

    public void setid(Integer id) {
        this.id = id;
    }
    
    public String getTimeStamp() {
        return time_stamp;
    }

    public void setTimeStamp(String time_stamp) {
        this.time_stamp = time_stamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String geticnumber() {
        return icnumber;
    }

    public void seticnumber(String icnumber) {
        this.icnumber = icnumber;
    }

    public String getContactNumber() {
        return contact_number;
    }

    public void setContactNumber(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFromDate() {
        return from_date;
    }

    public void setFromDate(String from_date) {
        this.from_date = from_date;
    }

    public String getToDate() {
        return to_date;
    }

    public void setToDate(String to_date) {
        this.to_date = to_date;
    }

    public Integer getGroupDefault() {
        return group_default;
    }

    public void setGroupDefault(Integer group_default) {
        this.group_default = group_default;
    }

    public String getGuestCompany() {
        return guest_company;
    }

    public void setGuestCompany(String guest_company) {
        this.guest_company = guest_company;
    }

    public Integer getReceptionUserID() {
        return reception_userid;
    }

    public void setReceptionUserID(Integer reception_userid) {
        this.reception_userid = reception_userid;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getVisitPurpose() {
        return visit_purpose;
    }

    public void setVisitPurpose(String visit_purpose) {
        this.visit_purpose = visit_purpose;
    }

    public String getVisitDepartment() {
        return visit_department;
    }

    public void setVisitDepartment(String visit_department) {
        this.visit_department = visit_department;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getmodifydatetime() {
        return modifydatetime;
    }

    public String getcreatedatetime() {
        return createdatetime;
    }

    public Integer getmodifyuserid() {
        return modifyuserid;
    }

    public Integer getcreateuserid() {
        return createuserid;
    }

    public void setmodifyuserid(Integer modifyuserid) {
        this.modifyuserid = modifyuserid;
    }

    public void setcreateuserid(Integer createuserid) {
        this.createuserid = createuserid;
    }

    public void setmodifydatetime(String modifydatetime) {
        this.modifydatetime = modifydatetime;
    }

    public void setcreatedatetime(String createdatetime) {
        this.createdatetime = createdatetime;
    }
}

