package code.java.workflow.WorkFlowCRUD;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import code.java.workflow.WorkFlowModel.WFTransaction;

public interface WFTransactionRepository extends JpaRepository<WFTransaction, Integer> {

    @Query(value = "select id FROM wfsetup_details wf where substring_index(substring_index(conditions,'Purpose=',-1),'AND',1) = :purpose AND substring_index(substring_index(conditions,'Department=',-1),'AND',1) = :department AND wf_id is null limit 1", nativeQuery = true)
    public Integer getSetupIdByPurposeDepartment(Integer purpose, Integer department);

    @Query(value = "select id FROM wfsetup_details wf where substring_index(substring_index(conditions,'Purpose=',-1),'AND',1) = :purpose AND substring_index(substring_index(conditions,'Department=',-1),'AND',1) = :department AND wf_id is not null", nativeQuery = true)
    public List<Integer> getSetupIdByPurposeDepartmentChild(Integer purpose, Integer department);
    
    
    @Query(value = "SELECT COALESCE(B.wf_id,A.id) FROM mydb.wfsetup_details A left JOIN mydb.wfsetup_details B ON B.wf_id = A.id where (substring_index(substring_index(A.conditions,'Purpose=',-1),'AND',1) = ?1 AND substring_index(substring_index(A.conditions,'Department=',-1),'AND',1) = 0) OR (substring_index(substring_index(A.conditions,'Purpose=',-1),'AND',1) = 0 AND substring_index(substring_index(A.conditions,'Department=',-1),'AND',1) = ?2) AND A.conditions NOT LIKE '%AND%' ",nativeQuery = true)
    public List<Integer> getSetupIDByORConditions(Integer purpose, Integer department);

    @Query(value="select id from workflow_transaction where visitorid = ?1 AND status='Pending' limit 1",nativeQuery = true)
    public Integer getNextStage(Integer id);

    @Transactional
    @Modifying
    @Query(value="update workflow_transaction set status = 'Reject' where visitorid = ?1 AND status = 'Pending'",nativeQuery = true)
    public void updateAllReject(Integer id);

    @Query(value="SELECT id from wfsetup_details where wf_id =?1",nativeQuery = true)
    public Integer getMatchId(Integer id);

    @Query(value="select * from workflow_transaction where visitorid = ?1",nativeQuery = true)
    public List<WFTransaction> getVisitorTransaction(Integer id);

    @Query(value="select * from workflow_transaction where visitorid = ?1",nativeQuery = true)
    public WFTransaction getVisitorTransaction2(Integer id);

    //view query get positionname and stage
    @Query(value="SELECT JSON_OBJECT('id',id , 'status',status, 'stage',stage,'visitorid',visitorid,'wfsetupid',wfsetup_id,'positionname',positionname) FROM view_wf_workflow_progressbar where visitorid = ?1",nativeQuery = true)
    public List<Object> getTransactions(Integer id);
}