package code.java.workflow.CRUD;

import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UsersController {

    @Autowired
    private UsersService service;

    @GetMapping("/list")
    public List<Users> list() {
        return service.listAll();
    }

    @GetMapping("/list/id={id}")
    public ResponseEntity<Users> get(@PathVariable Integer id) {
        try {
            Users users = service.get(id);
            return new ResponseEntity<Users>(users, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Users>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/id={id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);

    }
}