package code.java.workflow.WorkFlowCRUD;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import code.java.workflow.WorkFlowModel.WFTransaction;

@Service
@Transactional
public class WFTransactionService {

    @Autowired
    private WFTransactionRepository repo;

    public List<WFTransaction> listAll() {
        return repo.findAll();
    }

    public void save(WFTransaction workflow_transaction) {
        repo.save(workflow_transaction);
    }

    public WFTransaction get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }

    public Optional<WFTransaction> findById(Integer id) {
        return repo.findById(id);
    }

    public WFTransaction getVisitorTransaction2(Integer id){
        return repo.getVisitorTransaction2(id);
    }
    
}