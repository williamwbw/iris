package code.java.workflow.CRUD;

import javax.persistence.*;

@Entity
@Table(name = "empl_position")
public class EmplPosition {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String positionname;

	private String status;
	private Integer createuserid;
	private String createdatetime;
	private Integer modifyuserid;
	private String modifydatetime;

	public EmplPosition() {

	}

	public EmplPosition(Integer id, String positionname, String status,Integer createuserid, String createdatetime, Integer modifyuserid,
			String modifydatetime) {
		this.id = id;
		this.positionname = positionname;
		this.status=status;
		this.createuserid = createuserid;
		this.createdatetime = createdatetime;
		this.modifyuserid = modifyuserid;
		this.modifydatetime = modifydatetime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPositionName() {
		return positionname;
	}

	public void setName(String positionname) {
		this.positionname = positionname;
	}

	public String getmodifydatetime() {
		return modifydatetime;
	}

	public String getcreatedatetime() {
		return createdatetime;
	}

	public Integer getmodifyuserid() {
		return modifyuserid;
	}

	public Integer getcreateuserid() {
		return createuserid;
	}

	public void setmodifyuserid(Integer modifyuserid) {
		this.modifyuserid = modifyuserid;
	}

	public void setcreateuserid(Integer createuserid) {
		this.createuserid = createuserid;
	}

	public void setmodifydatetime(String modifydatetime) {
		this.modifydatetime = modifydatetime;
	}

	public void setcreatedatetime(String createdatetime) {
		this.createdatetime = createdatetime;
	}

	public String getstatus() {
        return status;
    }

	public void setStatus(String status) {
        this.status = status;
    }
}
