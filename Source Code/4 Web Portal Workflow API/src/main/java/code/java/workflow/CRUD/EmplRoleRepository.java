package code.java.workflow.CRUD;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmplRoleRepository extends JpaRepository<EmplRole, Integer> {

}