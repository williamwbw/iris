package code.java.workflow.WorkFlowCRUD;


import org.springframework.data.jpa.repository.JpaRepository;

import code.java.workflow.WorkFlowModel.WFSetupDetail;

public interface WFSetupDetailRepository extends JpaRepository<WFSetupDetail, Integer> {


}
