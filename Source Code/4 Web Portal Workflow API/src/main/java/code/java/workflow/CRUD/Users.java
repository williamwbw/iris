package code.java.workflow.CRUD;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Entity
@Table(	name = "users", 
		uniqueConstraints = { 
			@UniqueConstraint(columnNames = "username"),
			@UniqueConstraint(columnNames = "email") 
		})
public class Users {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 20)
	private String username;


	private String email;

	@NotBlank
	@Size(max = 120)
	private String password;

	@Size(max = 255)
	private String accessToken;
	private String status;
    private Integer createuserid;
    private String createdatetime;
    private Integer modifyuserid;
    private String modifydatetime;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "user_roles", 
				joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Roles> roles = new HashSet<>();

	public Users() {
	}

	public Users(String username, String email, String password , String accessToken, String status,
    Integer createuserid, String createdatetime, Integer modifyuserid, String modifydatetime) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.accessToken=accessToken;
		this.status = status;
        this.createuserid = createuserid;
        this.createdatetime = createdatetime;
        this.modifyuserid = modifyuserid;
        this.modifydatetime = modifydatetime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Roles> getRoles() {
		return roles;
	}

	public void setRoles(Set<Roles> roles) {
		this.roles = roles;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getstatus() {
        return status;
    }

    public String getmodifydatetime() {
        return modifydatetime;
    }

    public String getcreatedatetime() {
        return createdatetime;
    }

    public Integer getmodifyuserid() {
        return modifyuserid;
    }

    public Integer getcreateuserid() {
        return createuserid;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setmodifyuserid(Integer modifyuserid) {
        this.modifyuserid = modifyuserid;
    }

    public void setcreateuserid(Integer createuserid) {
        this.createuserid = createuserid;
    }

    public void setmodifydatetime(String modifydatetime) {
        this.modifydatetime = modifydatetime;
    }

    public void setcreatedatetime(String createdatetime) {
        this.createdatetime = createdatetime;
    }

}