package code.java.workflow.CRUD;


import org.springframework.data.jpa.repository.JpaRepository;

 
public interface EmplPositionRepository extends JpaRepository<EmplPosition, Integer> {

   
}