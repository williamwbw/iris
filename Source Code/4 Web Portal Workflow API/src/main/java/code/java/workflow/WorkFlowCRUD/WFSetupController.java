package code.java.workflow.WorkFlowCRUD;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import code.java.workflow.CRUD.Employees;
import code.java.workflow.CRUD.EmployeesRepository;
import code.java.workflow.WorkFlowModel.WFLog;
import code.java.workflow.WorkFlowModel.WFSetup;
import code.java.workflow.WorkFlowModel.WFSetupDetail;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/wfsetup")
public class WFSetupController {

    @Autowired
    private WFSetupService setupservice;

    @Autowired
    private WFSetupDetailService detailservice;

    @Autowired
    private EmployeesRepository employeerepo;

    @Autowired
    private WFSetupRepository setuprepo;

    @Autowired
    private WFLogService logservice;

    @GetMapping("/list/id={id}")
    public ResponseEntity<WFSetup> get(@PathVariable Integer id) {
        try {
            WFSetup workflow_setup = setupservice.get(id);
            return new ResponseEntity<WFSetup>(workflow_setup, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<WFSetup>(HttpStatus.NOT_FOUND);
        }
    }

    // list all setup details
    @GetMapping("/list")
    public List<Object> listAll() {
        return setuprepo.getAllSetup();
    }

    // list all setup details
    @GetMapping("/listAll")
    public List<Object> listAllWithoutFilter() {
        return setuprepo.getAllSetupWithoutFilter();
    }

    // add into workflowsetup
    @PostMapping("/add")
    public Object addWFSetup(@RequestBody WFSetup workflow_setup) {

        try{
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        workflow_setup.setcreatedatetime(formatter.format(date));
        workflow_setup.setmodifydatetime(formatter.format(date));
        workflow_setup.setConditions(workflow_setup.getConditions());
        workflow_setup.setStatus("Active");
        workflow_setup.setmodifyuserid(workflow_setup.getmodifyuserid());
        workflow_setup.setcreateuserid(workflow_setup.getcreateuserid());

        Object SavedUser = setuprepo.saveAndFlush(workflow_setup);

        // For Workflow log
        WFLog workflow_log = new WFLog();
        workflow_log.setcreatedatetime(formatter.format(date));
        workflow_log.setmodifydatetime(formatter.format(date));
        workflow_log.setcreateuserid(workflow_setup.getcreateuserid());
        workflow_log.setmodifyuserid(workflow_setup.getmodifyuserid());
        workflow_log.setAction("Create Workflow Setup");
        workflow_log.setRemarks(workflow_log.getRemarks());
        workflow_log.setRefTable("Workflow Setup");
        workflow_log.setRefId(workflow_setup.getId());

        logservice.save(workflow_log);

        return SavedUser;
        }catch(Exception ex){
            return ex.getMessage().toString();
        }

    }

    // update setup
    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody WFSetup workflow_setup, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            WFSetup existWorkflowSetup = setupservice.get(id);
            existWorkflowSetup.setTitle(workflow_setup.getTitle());
            existWorkflowSetup.setEmployeeID(workflow_setup.getEmployeeID());
            existWorkflowSetup.setConditions(workflow_setup.getConditions());
            existWorkflowSetup.setPosition(workflow_setup.getPosition());
            existWorkflowSetup.setRole(workflow_setup.getRole());
            existWorkflowSetup.setStatus(workflow_setup.getStatus());
            existWorkflowSetup.setmodifyuserid(workflow_setup.getmodifyuserid());
            existWorkflowSetup.setmodifydatetime(formatter.format(date));
            setupservice.save(existWorkflowSetup);

            // For Workflow log
            WFLog workflow_log = new WFLog();
            workflow_log.setAction("Update Workflow Setup");
            workflow_log.setcreatedatetime(formatter.format(date));
            workflow_log.setmodifydatetime(formatter.format(date));
            workflow_log.setcreateuserid(workflow_setup.getmodifyuserid());
            workflow_log.setmodifyuserid(workflow_setup.getmodifyuserid());
            workflow_log.setRefTable("Workflow Setup");
            workflow_log.setRefId(existWorkflowSetup.getId());

            logservice.save(workflow_log);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/updatedelete/id={id}")
    public ResponseEntity<?> updatedelete(@RequestBody WFSetup workflow_setup, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            WFSetup existWorkflowSetup = setupservice.get(id);
            existWorkflowSetup.setStatus(workflow_setup.getStatus());
            existWorkflowSetup.setmodifyuserid(workflow_setup.getmodifyuserid());
            existWorkflowSetup.setmodifydatetime(formatter.format(date));
            setupservice.save(existWorkflowSetup);

            Integer setup = setuprepo.getMatchId(id);
            WFSetup matchsetup = setupservice.get(setup);
            matchsetup.setStatus("Inactive");
            matchsetup.setmodifyuserid(workflow_setup.getmodifyuserid());
            matchsetup.setmodifydatetime(formatter.format(date));
            setupservice.save(matchsetup);

            // For Workflow log
            WFLog workflow_log = new WFLog();
            workflow_log.setAction("Delete Workflow Setup");
            workflow_log.setcreatedatetime(formatter.format(date));
            workflow_log.setmodifydatetime(formatter.format(date));
            workflow_log.setcreateuserid(workflow_setup.getmodifyuserid());
            workflow_log.setmodifyuserid(workflow_setup.getmodifyuserid());
            workflow_log.setRefTable("Workflow Setup");
            workflow_log.setRefId(existWorkflowSetup.getId());

            logservice.save(workflow_log);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // update workflow detail
    @PutMapping("/updatedetails/id={id}")
    public ResponseEntity<?> updateWFDetail(@RequestBody WFSetupDetail wfsetup_details, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            WFSetupDetail existWFSetupDetail = detailservice.get(id);
            existWFSetupDetail.setTitle(wfsetup_details.getTitle());
            existWFSetupDetail.setEmployeeID(wfsetup_details.getEmployeeID());
            existWFSetupDetail.setRole(wfsetup_details.getRole());
            existWFSetupDetail.setPosition(wfsetup_details.getPosition());
            // existWFSetupDetail.setmodifydatetime(formatter.format(date));
            detailservice.save(existWFSetupDetail);

            // For Workflow log
            WFLog workflow_log = new WFLog();
            workflow_log.setAction("Update Workflow Setup");
            workflow_log.setcreatedatetime(formatter.format(date));
            workflow_log.setmodifydatetime(formatter.format(date));
            //workflow_log.setmodifyuserid(wfsetup_details.getmodifyuserid());
            workflow_log.setRefTable("Workflow Setup");
            workflow_log.setRefId(existWFSetupDetail.getId());

            logservice.save(workflow_log);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id) {
        setupservice.delete(id);
    }

    // role and position find match employees return id,name,position,role

    @GetMapping("/getemployee/role={role}/position={position}")
    public List<Object> getEmplos(@PathVariable("role") String role, @PathVariable("position") String position) {
        List<Object> employees;
        if (role.equals("0")) {
            employees = employeerepo.getEmployeeIdAndNamesByPosition(position);
        } else if (position.equals("0")) {
            employees = employeerepo.getEmployeeIdAndNamesByRole(role);
        } else {
            employees = employeerepo.getEmployeeIdAndNames(role, position);
        }

        return employees;

    }


}
