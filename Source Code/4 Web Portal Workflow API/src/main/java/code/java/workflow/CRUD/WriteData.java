package code.java.workflow.CRUD;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import org.apache.tomcat.util.json.ParseException;
import org.json.*;

public class WriteData {
    
    public static Connection ConnectToDB() throws Exception {
        // Registering the Driver
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        // Getting the connection
        String mysqlUrl = "jdbc:mysql://localhost:3306/mydb";
        Connection con = DriverManager.getConnection(mysqlUrl, "root", "New5998.");
        System.out.println("Connection established......");
        return con;
    }

    public static void getdata(String data) {
        // String pattern = "yyyy-MM-dd HH:mm:ss";
        // SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        //DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            // Retrieving the array
            JSONArray jsonArray = new JSONArray(data);

            //Connect to database
            Connection con = ConnectToDB();

            //Arrange and add the data into the database using preparestatement
            PreparedStatement pstmt = con.prepareStatement(
                    "INSERT INTO visitors (time_stamp,name,gender,ICNumber,contact_number,email,from_date,to_date,group_default,guest_company,reception_userid,remarks,visit_purpose,visit_department) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            for (Object object : jsonArray) {
                JSONObject record = (JSONObject) object;
                String Timestamp = record.get("TimeStamp").toString();
                String Name = record.get("Name").toString();
                String Gender = record.get("Gender").toString();
                String ICNumber = record.get("ICNumber").toString();
                String ContactNumber = record.get("ContactNumber").toString();
                String Email = record.get("Email").toString();
                String From = record.get("From").toString();
                String To = record.get("To").toString();
                int Group = Integer.parseInt( record.get("Group").toString());
                String GuestCompany = record.get("GuestCompany").toString();
                int ReceptionUserID = Integer.parseInt(record.get("ReceptionUserID").toString());
                String Remarks = record.get("Remarks").toString();
                String VisitPurpose = record.get("VisitPurpose").toString();
                String VisitDepartment = record.get("VisitDepartment").toString();
                pstmt.setString(1, Timestamp);
                pstmt.setString(2, Name);
                pstmt.setString(3, Gender);
                pstmt.setString(4, ICNumber);
                pstmt.setString(5, ContactNumber);
                pstmt.setString(6, Email);
                pstmt.setString(7, From);
                pstmt.setString(8, To);
                pstmt.setInt(9, Group);
                pstmt.setString(10, GuestCompany);
                pstmt.setInt(11, ReceptionUserID);
                pstmt.setString(12, Remarks);
                pstmt.setString(13, VisitPurpose);
                pstmt.setString(14, VisitDepartment);
                pstmt.executeUpdate();
            }
            System.out.println("Records inserted.....");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}
