package code.java.workflow.WorkFlowCRUD;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import code.java.workflow.WorkFlowModel.WFSetup;

public interface WFSetupRepository extends JpaRepository<WFSetup, Integer> {

    // /JSON_OBJECT('id',employees.id, 'name',employees.name)
    @Query(value = "SELECT JSON_OBJECT('id',wf.id , 'title',wf.title, 'employeename',group_concat(em.name) , 'purpose',po.purposename, 'department',de.departmentname) FROM wfsetup_details  wf left join employees em on find_in_set(em.id,wf.employeeids) left join purpose po on substring_index(substring_index(conditions,'Purpose=',-1),'AND',1)  = po.id left join department de on substring_index(substring_index(conditions,'Department=',-1),'AND',1) = de.id", nativeQuery = true)
    public List<Object> getAllSetup();

    @Query(value = "SELECT JSON_OBJECT('id',wf.id , 'title',wf.title, 'employeename',group_concat(em.id) , 'purpose',po.id, 'department',de.id) FROM wfsetup_details  wf left join employees em on find_in_set(em.id,wf.employeeids) left join purpose po on substring_index(substring_index(conditions,'Purpose=',-1),'AND',1)  = po.id left join department de on substring_index(substring_index(conditions,'Department=',-1),'AND',1) = de.id", nativeQuery = true)
    public List<Object> getAllSetupWithoutFilter();

    @Query(value="SELECT id from wfsetup_details where wf_id =?1",nativeQuery = true)
    public Integer getMatchId(Integer id);

}
