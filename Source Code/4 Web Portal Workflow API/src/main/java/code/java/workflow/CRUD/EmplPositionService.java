package code.java.workflow.CRUD;

import java.util.List;

import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class EmplPositionService {
    @Autowired
    private EmplPositionRepository repo;
     
    public List<EmplPosition> listAll() {
        return repo.findAll();
    }
     
    public void save(EmplPosition empl_position) {
        repo.save(empl_position);
    }
     
    public EmplPosition get(Integer id) {
        return repo.findById(id).get();
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }
  
   
}
