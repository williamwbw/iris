package code.java.workflow.CRUD;

import java.util.List;
import java.util.NoSuchElementException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/employees")
public class EmployeesController {

    @Autowired
    private EmployeesService service;

    // @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/list")
    public List<Employees> list() {
        return service.listAll();
    }

    // @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/list/{id}")
    public ResponseEntity<Employees> get(@PathVariable Integer id) {
        try {
            Employees employees = service.get(id);
            return new ResponseEntity<Employees>(employees, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Employees>(HttpStatus.NOT_FOUND);
        }
    }

    // @CrossOrigin(origins = "http://localhost:8080")
    @PostMapping("/add")
    public void add(@RequestBody Employees employees) {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        employees.setcreatedatetime(formatter.format(date));
        employees.setmodifydatetime(formatter.format(date));
        service.save(employees);
    }

    // @CrossOrigin(origins = "http://localhost:8080")
    @PutMapping("/update/id={id}")
    public ResponseEntity<?> update(@RequestBody Employees employees, @PathVariable Integer id) {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            Employees existEmployees = service.get(id);
            existEmployees.setName(employees.getName());
            existEmployees.setUsername(employees.getUsername());
            existEmployees.setDepartment(employees.getDepartment());
            //existEmployees.setPosition(employees.getPosition());
            existEmployees.setStatus(employees.getstatus());
            existEmployees.setmodifyuserid(employees.getmodifyuserid());
            existEmployees.setmodifydatetime(formatter.format(date));
            service.save(existEmployees);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // @CrossOrigin(origins = "http://localhost:8080")
    @DeleteMapping("/delete/id={id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}