package code.java.workflow.WorkFlowModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "workflow_setup")
public class WFSetup extends WFSetupDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String conditions;
    private String status;
    private Integer createuserid;
    private String createdatetime;
    private Integer modifyuserid;
    private String modifydatetime;

    public WFSetup() {
    }

    public WFSetup(Integer id, String conditions, String status, Integer createuserid, String createdatetime,
            Integer modifyuserid, String modifydatetime) {

        this.id = id;
        this.conditions = conditions;
        this.status = "Active";
        this.createuserid = createuserid;
        this.createdatetime = createdatetime;
        this.modifyuserid = modifyuserid;
        this.modifydatetime = modifydatetime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getmodifydatetime() {
        return modifydatetime;
    }

    public String getcreatedatetime() {
        return createdatetime;
    }

    public Integer getmodifyuserid() {
        return modifyuserid;
    }

    public Integer getcreateuserid() {
        return createuserid;
    }

    public void setmodifyuserid(Integer modifyuserid) {
        this.modifyuserid = modifyuserid;
    }

    public void setcreateuserid(Integer createuserid) {
        this.createuserid = createuserid;
    }

    public void setmodifydatetime(String modifydatetime) {
        this.modifydatetime = modifydatetime;
    }

    public void setcreatedatetime(String createdatetime) {
        this.createdatetime = createdatetime;
    }

}
