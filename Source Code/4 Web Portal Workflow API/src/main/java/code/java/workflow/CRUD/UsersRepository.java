package code.java.workflow.CRUD;

import org.springframework.data.jpa.repository.JpaRepository;

 
public interface UsersRepository extends JpaRepository<Users, Integer> {
 
}