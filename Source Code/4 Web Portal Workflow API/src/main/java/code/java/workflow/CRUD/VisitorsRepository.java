package code.java.workflow.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface VisitorsRepository extends JpaRepository<Visitors, Integer> {

    @Query(value = "select v.name,v.visit_purpose, v.visit_department from visitors v ", nativeQuery = true)
    public List<Object> getVisitPurposeAndDepartment();

    @Query(value = "select v.name,v.visit_purpose from visitors v ", nativeQuery = true)
    public List<Object> getVisitPurpose();

    @Query(value = "select v.name,v.visit_department from visitors v ", nativeQuery = true)
    public List<Object> getVisitDepartment();

    // return Visitor Check In Out List when status pending
    @Query(value = "select JSON_OBJECT('id',v.id ,'name', v.name, 'icnumber', v.icnumber,'time_stamp', v.time_stamp, 'from_date',v.from_date, 'to_date',v.to_date, 'status',v.status, 'purposename', po.purposename ,'departmentname', de.departmentname) FROM visitors  v left join purpose po on po.id  = v.visit_purpose left join department de on de.id = v.visit_department where v.status = 'Pending' ", nativeQuery = true)
    public List<Object> getVisitors();

    // get specific visitor
    @Query(value = "select JSON_OBJECT('id',v.id ,'Name', v.name, 'ICNumber', v.icnumber,'TimeStamp', v.time_stamp, 'Fromdate',v.from_date, 'ToDate',v.to_date, 'Status',v.status, 'PurposeName', po.purposename ,'DepartmentName', de.departmentname) FROM visitors  v left join purpose po on po.id  = v.visit_purpose left join department de on de.id = v.visit_department where v.id =?1 ", nativeQuery = true)
    public List<Object> getSpecificVisitor(Integer id);

    // // return all visitor
    // @Query(value = "select JSON_OBJECT('id',v.id ,'name', v.name, 'icnumber', v.icnumber,'time_stamp', v.time_stamp, 'from_date',v.from_date, 'to_date',v.to_date, 'status',v.status, 'visit_purpose',v.visit_purpose, 'departmentname', de.departmentname) FROM visitors  v left join department de on de.id = v.visit_department", nativeQuery = true)
    // public List<Object> getAllVisitors();

    // return all visitor
    @Query(value = "select v.id, v.name, v.icnumber, v.time_stamp, v.from_date, v.to_date, v.status, v.visit_purpose, de.departmentname FROM visitors  v left join department de on de.id = v.visit_department", nativeQuery = true)
    public List<Object> getAllVisitors();

    // @Query("SELECT * FROM blacklists WHERE username = ?1")
    // public List<Blacklist> findByUsername(String username); 

    // @Query("SELECT * FROM blacklists WHERE icpassport = ?1")
    // public List<Blacklist> findByIC(String icpassport);

}
