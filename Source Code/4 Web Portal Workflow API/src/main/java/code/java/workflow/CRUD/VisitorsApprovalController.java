package code.java.workflow.CRUD;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import java.io.UnsupportedEncodingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import code.java.workflow.WorkFlowCRUD.WFTransactionService;
import code.java.workflow.WorkFlowModel.WFTransaction;

@RestController
@RequestMapping("/api/visitorapproval")
public class VisitorsApprovalController {
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private VisitorsService service;

    @Autowired
    private WFTransactionService wfservice;

    public void sendEmailApprove(String recipientEmail, String recipientName)
    throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();              
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("support@iris.com", "Iris Support");
        helper.setTo(recipientEmail);
        
        String subject = "Visitor Approval Status";
        
        String content = "<p>Hello "+recipientName+",</p>"
                + "<p>Congratulation!</p>"
                + "<p>We're here to inform you that your request has been successfully approved!</p>"
                + "<br>"
                + "<p>This email is auto generated </p>";
        
        helper.setSubject(subject);
        
        helper.setText(content, true);
        
        mailSender.send(message);
        }

    public void sendEmailReject(String recipientEmail, String recipientName) throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();              
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("support@iris.com", "Iris Support");
        helper.setTo(recipientEmail);
        
        String subject = "Visitor Approval Status";
        
        String content = "<p>Hello "+recipientName+",</p>"
                + "<p>We're sorry to inform you that your request has been rejected!</p>"
                + "<br>"
                + "<p>This email is auto generated </p>";
        
        helper.setSubject(subject);
        
        helper.setText(content, true);
        
        mailSender.send(message);
        }
    
        
    @CrossOrigin(origins = "http://localhost:8080")
    @PutMapping("/approve/id={id}")
    public ResponseEntity<?> approve(@RequestBody Visitors visitors, @PathVariable Integer id) throws UnsupportedEncodingException, MessagingException {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            String email = visitors.getEmail();
            
           
            Visitors existVisitors = service.get(id);
            new Thread(() -> {
                try{
                    sendEmailApprove(email,existVisitors.getName());
                }catch(Exception e){

                }
                
            }).start();
            existVisitors.setRemarks(visitors.getRemarks());
            existVisitors.setStatus(visitors.getStatus());
            existVisitors.setmodifyuserid(visitors.getmodifyuserid());
            existVisitors.setmodifydatetime(formatter.format(date));
            service.save(existVisitors);
            
            // update table workflow transaction
            WFTransaction existWorkflowTransaction = wfservice.getVisitorTransaction2(id);
            existWorkflowTransaction.setStatus(visitors.getStatus());
            existWorkflowTransaction.setRemarks("Approved");
            existWorkflowTransaction.setmodifyuserid(visitors.getmodifyuserid());
            existWorkflowTransaction.setmodifydatetime(formatter.format(date));
            wfservice.save(existWorkflowTransaction);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

        @CrossOrigin(origins = "http://localhost:8080")
    @PutMapping("/reject/id={id}")
    public ResponseEntity<?> reject(@RequestBody Visitors visitors, @PathVariable Integer id) throws UnsupportedEncodingException, MessagingException {
        try {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            String email = visitors.getEmail();
            Visitors existVisitors = service.get(id);
            new Thread(() -> {
                try{
                    sendEmailReject(email,existVisitors.getName());
                }catch(Exception e){

                }                
            }).start();
            existVisitors.setRemarks(visitors.getRemarks());
            existVisitors.setStatus(visitors.getStatus());
            existVisitors.setmodifyuserid(visitors.getmodifyuserid());
            existVisitors.setmodifydatetime(formatter.format(date));
            service.save(existVisitors);

            // update table workflow transaction
            WFTransaction existWorkflowTransaction = wfservice.getVisitorTransaction2(id);
            existWorkflowTransaction.setStatus("Reject");
            existWorkflowTransaction.setRemarks("Rejected");
            existWorkflowTransaction.setmodifyuserid(visitors.getmodifyuserid());
            existWorkflowTransaction.setmodifydatetime(formatter.format(date));
            wfservice.save(existWorkflowTransaction);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
