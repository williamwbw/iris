package code.java.workflow.CRUD;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WriteDataController {
    
    @PostMapping(value = "/api/visitorsData", consumes = { MediaType.ALL_VALUE })
    public String googleFormData(@RequestBody String JA) throws Exception {
        //get the function to run data inside JA(google form data)
        WriteData.getdata(JA);
        //System.out.println(JA);
        return "1";
    }

}
