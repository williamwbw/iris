const roleData = [
    { id:'1', rolename: 'Manager'},
    { id:'2', rolename: 'Sales Manager'},
    { id:'3', rolename: 'Development Manager'},
    { id:'4', rolename: 'HR Manager'},
    { id:'5', rolename: 'CEO'},
  ]
  
  export default roleData