const usersData = [
  { username: 'Samppa Nori', CheckIn: '2012/01/01 13:23:00', Department: 'Sales', Purpose: 'Discussion', status: 'Pending'},
  { username: 'Estavan Lykos', CheckIn: '2012/02/01 13:23:00', Department: 'General Office', Purpose: 'Training', status: 'Pending'},
  { username: 'Chetan Mohamed', CheckIn: '2012/02/01 13:23:00', Department: 'Sales', Purpose: 'Meeting', status: 'Pending'},
  { username: 'Derick Maximinus', CheckIn: '2012/03/01 13:23:00', Department: 'Other', Purpose: 'Other', status: 'Pending'},
  { username: 'Friderik Dávid', CheckIn: '2012/01/21 13:23:00', Department: 'General Office', Purpose: 'Discussion', status: 'Pending'},
  { username: 'Yiorgos Avraamu', CheckIn: '2012/01/01 13:23:00', Department: 'Legal', Purpose: 'Meeting', status: 'Pending'},
  { username: 'Avram Tarasios', CheckIn: '2012/02/01 13:23:00', Department: 'General Office', Purpose: 'Discussion', status: 'Pending'},
  { username: 'Quintin Ed', CheckIn: '2012/02/01 13:23:00', Department: 'Legal', Purpose: 'Training', status: 'Pending'}
]

export default usersData


