const visitorData = [
    { id:'1', Name: 'John', ICNumber: '000000-00-0000', TimeStamp: '13/04/2021 16:52:00', Department:'HR', Purpose:'Interview', From:'14/04/2021 14:30:00', To:'14/04/2021 15:30:00', status: 'Pending' },
    { id:'2', Name: 'Jane', ICNumber: '000000-00-0000', TimeStamp: '13/04/2021 16:52:00', Department:'Sales', Purpose:'Meeting', From:'14/04/2021 14:30:00', To:'14/04/2021 15:30:00', status: 'Pending' },
    { id:'3', Name: 'Alex', ICNumber: '000000-00-0000', TimeStamp: '13/04/2021 16:52:00', Department:'Development', Purpose:'Meeting', From:'14/04/2021 14:30:00', To:'14/04/2021 15:30:00', status: 'Pending' },
    { id:'4', Name: 'Joe', ICNumber: '000000-00-0000', TimeStamp: '13/04/2021 16:52:00', Department:'HR', Purpose:'Recuirment', From:'14/04/2021 14:30:00', To:'14/04/2021 15:30:00', status: 'Pending' },
    { id:'5', Name: 'May', ICNumber: '000000-00-0000', TimeStamp: '13/04/2021 16:52:00', Department:'Sales', Purpose:'Purchase', From:'14/04/2021 14:30:00', To:'14/04/2021 15:30:00', status: 'Pending' },
    { id:'6', Name: 'Jolis', ICNumber: '000000-00-0000', TimeStamp: '13/04/2021 16:52:00', Department:'HR', Purpose:'Repairing', From:'14/04/2021 14:30:00', To:'14/04/2021 15:30:00', status: 'Pending' },
  ]
  
  export default visitorData