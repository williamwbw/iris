const approverData = [
    { approver: 'Manager'},
    { approver: 'Sales Manager'},
    { approver: 'Development Manager'},
    { approver: 'CEO'},
  ]
  
  export default approverData