export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
        _name: 'CSidebarNavItem',
        name: 'Dashboard',
        to: '/Dashboard',
        icon: 'cil-speedometer'
      },
      {
        _name: 'CSidebarNavItem',
        name: 'CheckInOut',
        to: '/CheckInOut',
        icon: 'cilSwapHorizontal'
      },
      {
        _name: 'CSidebarNavItem',
        name: 'Approve Visitor List',
        to: '/visitorList',
        icon: 'cilCheckCircle'
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Maintenance',
        to: '/Maintain',
        icon: 'cilAppsSettings',
        items: [
          {
              name: 'Blacklist',
              to: '/Blacklist'
          },
          {
            name: 'Register Employee',
            to: '/Register'
          },
          {
            name: 'Users',
            to: '/NewUser'
          },
          
          // {
          //   name: 'Department',
          //   to: '/Department'
          // },
          // {
          //   name: 'Purpose',
          //   to: '/Purpose'
          // },
          // {
          //   name: 'Roles',
          //   to: '/role'
          // },
          // {
          //   name: 'Position',
          //   to: '/position'
          // },
          // {
          //   name: 'API Configuration',
          //   to: '/APICon'
          // },
 
          {
            name: 'Configuration',
            to: '/ConfigurationPage4'
          },
        ]
        
      },
        // {
        //     _name: 'CSidebarNavDropdown',
        //     name: 'Configuration',
        //     to: '/configuration',
        //     icon: 'cilSettings',
        //     items: [
        //       {
        //         name: 'Workflow Setup',
        //         to: '/workflowSetting'
        //       },
        //     ]
        // },
      {
          _name: 'CSidebarNavDropdown',
          name: 'Report',
          to: '/report',
          icon: 'cilLayers',
          items: [
            {
              name: 'In/Out Report',
              to: '/InOutReport'
            },
            {
              name: 'Blacklist Report',
              to: '/BlacklistReport'
            },
            {
              name: 'Approval Action Report',
              to: '/ApprovalActionReport'
            },
            
          ]
      }
    ]
  }
]