import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views
const dashboard = () => import('@/views/dashboard/dashboard')
//const User = () => import('@/views/users/User')

//Pages
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')
const ForgotPassword = () => import('@/views/pages/ForgotPassword')
const ResetPassword= () => import('@/views/pages/ResetPassword')

//CheckInOut
const CInOut = () => import('@/views/dashboard/CInOut')

// Maintain
const Blacklist = () => import('@/views/maintain/Blacklist')
const Department = () => import('@/views/maintain/Department')
const Purpose = () => import('@/views/maintain/Purpose')
const NewUser = () => import('@/views/maintain/NewUser')
const APICon = () => import('@/views/maintain/ApiCon')
const role = () => import('@/views/maintain/role')
const position = () => import('@/views/maintain/position')
const ConfigurationPage = () => import('@/views/maintain/ConfigurationPage')
const ConfigurationPage4 = () => import('@/views/maintain/ConfigurationPage4')

// Settings
//const Setting = () => import('@/views/dashboard/SettingPage')

//Report
const InOutReport = () => import('@/views/report/InOutReport')
const BlacklistReport = () => import('@/views/report/BlacklistReport')
const ApprovalActionReport = () => import('@/views/report/ApprovalActionReport')

//Workflow
const approvePage = () => import('@/views/workflow/approvePage')
  // removed by Jacky - not needed as workflow not required
  // const approvePage2 = () => import('@/views/workflow/approvePage2')
  // const approvePage3 = () => import('@/views/workflow/approvePage3')
  // const approvePage4 = () => import('@/views/workflow/approvePage4')
const visitorList = () => import('@/views/workflow/visitorList')
const visitorApproval = () => import('@/views/workflow/visitorApproval')
const visitorDetails = () => import('@/views/workflow/visitorDetails')
const workflowSetting = () => import('@/views/workflow/workflowSetting')
const workflowSetup = () => import('@/views/workflow/workflowSetup')
const modifySetup = () => import('@/views/workflow/modifySetup')

// User 
const Profile = () => import('@/views/pages/Profile')
const ChangePasswordCont = () => import('@/views/pages/ChangePasswordCont')




Vue.use(Router)

export const router = new Router({
 // mode: 'history',
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
  return [
    {
      path: '/',
      redirect: '/Login',
      name: 'Home',
      component: Login,
      children: [

        {
          path: 'login',
          name: 'Login',
          component: Login
        },
      ]
    },
    {
      path: '/ForgotPassword',
      name: 'ForgotPassword',
      component: ForgotPassword
    },
    {
      path: '/',
      name: '/ResetPassword',
      component: ResetPassword,
      children: [
        {
          path: 'ResetPassword',
          name: 'ResetPassword',
          component: ResetPassword
        }
      ]
    },
    {
      path: '/',
      redirect: '/Home/Dashboard',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          component: dashboard
        },
        {
          path: 'CheckInOut',
          name: 'CheckInOut',
          component: CInOut
        },
        {
          path: 'Blacklist',
          name: 'Blacklist',
          component: Blacklist
        },
        {
          path: 'NewUser',
          name: 'NewUser',
          component: NewUser
        },
        {
          path: 'Register',
          name: 'Register',
          component: Register
        },        
        {
          path: 'Department',
          name: 'Department',
          component: Department
        },
        {
          path: 'Purpose',
          name: 'Purpose',
          component: Purpose
        },
        {
          path: 'APICon',
          name: 'APICon',
          component: APICon
        },
        {
          path: 'ConfigurationPage',
          name: 'ConfigurationPage',
          component: ConfigurationPage
        },
        {
          path: 'ConfigurationPage4',
          name: 'ConfigurationPage4',
          component: ConfigurationPage4
        },
        {
          path: 'role',
          name: 'role',
          component: role
        },
        {
          path: 'position',
          name: 'position',
          component: position
        },
        {
          path: 'InOutReport',
          name: 'InOutReport',
          component: InOutReport
        },
        {
          path: 'BlacklistReport',
          name: 'BlacklistReport',
          component: BlacklistReport
        },
        {
          path: 'ApprovalActionReport',
          name: 'ApprovalActionReport',
          component: ApprovalActionReport
        },
        {
          path: 'Profile',
          name: 'Profile',
          component: Profile
        },
        {
          path: 'ChangePasswordCont',
          name: 'ChangePasswordCont',
          component: ChangePasswordCont
        },
        // {
        //   path: 'approvePage',
        //   name: 'approvePage',
        //   component: approvePage
        // },
        {
          path: 'visitorList',
          name: 'visitorList',
          component: visitorList
        },
        {
          path: 'visitorApproval',
          name: 'visitorApproval',
          component: visitorApproval
        },
        {
          path: 'visitorDetails',
          name: 'visitorDetails',
          component: visitorDetails
        },
        {
          path: 'approvePage/:id',
          meta: {
            label: 'Visitor Details'
          },
          name: 'approvePage',
          prop:true,
          component: approvePage
        },
        //  removed By: Jacky
        //  reason: workflow setup remove. and approvePage2/3/4 seems to be used in workflow setup
        // {
        //   path: 'approvePage2/:id',
        //   meta: {
        //     label: 'Visitor Details'
        //   },
        //   name: 'approvePage',
        //   component: approvePage2
        // },
        // {
        //   path: 'approvePage3/:id',
        //   meta: {
        //     label: 'Visitor Details'
        //   },
        //   name: 'approvePage',
        //   component: approvePage3
        // },
        // {
        //   path: 'approvePage4/:id',
        //   meta: {
        //     label: 'Visitor Details'
        //   },
        //   name: 'approvePage',
        //   component: approvePage4
        // },
        {
          path: 'workflowSetting',
          name: 'workflowSetting',
          component: workflowSetting
        },
        {
          path: 'workflowSetup',
          name: 'workflowSetup',
          component: workflowSetup
        },
        {
          path: 'modifySetup/:id',
          meta: {
            label: 'Modify Workflow'
          },
          name: 'modify Setup',
          component: modifySetup
        },
      ]
    },
    {
      path:'/:pathMatch(.*)*',
      name:"NotFound",
      component:()=>import(/* webpackChunkName: "NotFound" */ '@/views/pages/Page404'),
    }
  ]
}

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/ForgotPassword', '/ResetPassword'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = sessionStorage.getItem('user');

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next('/login');
  } else {
    next();
  }
});

export default router;