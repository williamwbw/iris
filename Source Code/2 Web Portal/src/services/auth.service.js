import axios from 'axios';

const API_URL = 'http://localhost:1235/api/auth/';

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'signin', {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          sessionStorage.setItem('user', JSON.stringify(response.data));
          //var token = response.data.accessToken;
        }

        return response.data;
      });
  }

  logout(user) {
    //alert("hi")
    //alert("user id " + user.id)
    return axios
    .post(API_URL + 'logout',{
      username: user.username,
      id:user.id,
      accessToken: user.accessToken
    })
    .then(response =>{
    // alert('Logout Successful'),
    sessionStorage.removeItem('user'); 
      
    return response.data;

    });
   
    
  }

  register(user) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password,
      role:[user.role]
    });
  }
}

export default new AuthService();
