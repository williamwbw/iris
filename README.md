# README #

This document readme is for IRIS geo time projects

### Video Link
https://screenrec.com/share/fQ0whG7POm
https://screenrec.com/share/0IXElKkQAd


### What is this repository for? ###

* Documentation & source code

### How do I get set up? ###

### Tools to use ###
1: Visual Studio Code

2: Spring boots Extension

3: Mysql Workbench


### Setup for Local Environment ###
1: Install Java (https://www.java.com/en/download/) into windows (If using windows, if using unbuntu, please follow customer setup step)

2: Download & install JDK 1.8 (https://www.oracle.com/java/technologies/downloads/#java8)

3: After install java and jdk 1.8, use command (java --version) to check version

4: Expected result will be shown java version as "1.8.x_xxx"

5: Install JRE 8 (https://www.oracle.com/java/technologies/downloads/#java8)

6: After install, follow setup step as below

### JRE Setup Step ###
Step 1: Open C Drive > Program Files folder and look for Java folder and open it.

Step 2: Inside the Java folder you will find a jre1.8.0_291 folder, open it and open the bin folder, copy the path of the bin folder.

Note:- Now you have to set the environment variable.

Step 3: For that Go to Control Panel > System and Security > System > Advanced System Setting > Environment Variables

Step 4: Under the System variable, you will find “PATH” just select it and click on edit. Now click on New and add the bin folder location, after that click on “OK“.

7: Download MYSQL (https://dev.mysql.com/downloads/installer/), and install it

8: Download MYSQL Workbench (https://dev.mysql.com/downloads/workbench/)

9: Restore the database release script into database

### Setup for Customer Environment ###
1: Install unbuntu for customer

2: Install Mysql and MYsql workbench for customer

3: Running command below

Command
apt-get update -y
apt-get upgrade -y
apt-get install openjdk-8-jdk -y
java -version
curl -s https://get.sdkman.io | bash
source "/root/.sdkman/bin/sdkman-init.sh"
sdk help
sdk install springboot
spring version
sdk install gradle 4.5.1

sudo nano /etc/systemd/system/IrisUserRest.service
create a systemd service file to manage your application

Sample Script
[Unit]
Description=Spring Boot IrisUserRest
After=syslog.target
After=network.target[Service]
User=root
Type=simple

[Service]
ExecStart=/usr/bin/java -jar /home/user/UsersREST_latestv4/target/UsersREST-0.0.1
Restart=always
StandardOutput=syslog
StandardError=syslog

[Install]
WantedBy=multi-user.target

sudo systemctl daemon-reload
systemctl enable IrisUserRest
systemctl start IrisUserRest
systemctl status IrisUserRest

### Spring Boots VSC Installation ##

### Name: Spring Boot Tools 

Id: pivotal.vscode-spring-boot

Description: Provides validation and content assist for Spring Boot `application.properties`, `application.yml` properties files. As well as Boot-specific support for `.java` files.

Version: 1.28.0

Publisher: Pivotal

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-spring-boot


### Name: Spring Initializr Java Support  

Id: vscjava.vscode-spring-initializr

Description: A lightweight extension based on Spring Initializr to generate quick start Spring Boot Java projects.

Version: 0.7.0

Publisher: Microsoft

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-spring-initializr


### Name: Spring Boot Dashboard  

Id: vscjava.vscode-spring-boot-dashboard

Description: Spring Boot Dashboard for VS Code

Version: 0.2.0

Publisher: Microsoft

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-spring-boot-dashboard


### Name: Spring Boot Extension Pack 

Id: pivotal.vscode-boot-dev-pack

Description: A collection of extensions for developing Spring Boot applications

Version: 0.1.0

Publisher: Pivotal

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-boot-dev-pack


### Name: Extension Pack for Java 

Id: vscjava.vscode-java-pack

Description: Popular extensions for Java development that provides Java IntelliSense, debugging, testing, Maven/Gradle support, project management and more

Version: 0.18.5

Publisher: Microsoft

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack



### Vue JS Installation ###
Step 1: Download Node.js Installer (https://nodejs.org/en/download/)

Step 2: Install Node.js and NPM from Browser

1. Once the installer finishes downloading, launch it. Open the downloads link in your browser and click the file. Or, browse to the location where you have saved the file and double-click it to launch.

2. The system will ask if you want to run the software – click Run.

3. You will be welcomed to the Node.js Setup Wizard – click Next.

4. On the next screen, review the license agreement. Click Next if you agree to the terms and install the software.

5. The installer will prompt you for the installation location. Leave the default location, unless you have a specific need to install it somewhere else – then click Next.

6. The wizard will let you select components to include or remove from the installation. Again, unless you have a specific need, accept the defaults by clicking Next.

7. Finally, click the Install button to run the installer. When it finishes, click Finish.

Step 3: Verify Installation
Open a command prompt (or PowerShell), and enter the following:

node -v
The system should display the Node.js version installed on your system. You can do the same for NPM:

npm -v
Testing Node JS and NPM on Windows using CMD

Step 4: npm install vue

Step 5: npm install -g @vue/cli

### After open the PROJECT of VUEJS, please open up the terminal, and type in command [ NPM i ], this is to install all extension used in selected project.

Command to run vue project for development = npm run serve

Command to build vue project for production = npm run build